﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WolfReColor {
	public partial class NumberRange : Form {
		public decimal FromNumber { 
			get { return fromNumber.Value; }
			set { fromNumber.Value = value; }
		}

		public decimal ToNumber { 
			get { return toNumber.Value; }
			set { toNumber.Value = value; }
		}

		public decimal Maximum {
			get { return toNumber.Maximum; }
			set {
				toNumber.Maximum = value;
				fromNumber.Maximum = value;
			}
		}

		public decimal Minimum {
			get { return toNumber.Minimum; }
			set {
				toNumber.Minimum = value;
				fromNumber.Minimum = value;
			}
		}

		public DataPage[] Images { private get; set; }

		public Dictionary<byte, PixelMap> PreviewMap { private get; set; }

		public int ExportExtension {
			get {
				return exportFormatBox.SelectedIndex;
			}
		}

		public int ExportImageType { get { return exportImageTypeBox.SelectedIndex; } }

		public int Zoom { get { return zoomBar.Value; } }

		bool dirty { get; set; }

		bool loaded { get; set; }

		Bitmap cachedCanvas { get; set; }

		private int zoom {
			get {
				return zoomBar.Value;
			}
		}
		Dictionary<int, Bitmap> cachedImages { get; set; }

		public NumberRange(bool hideExportOptions) {
			InitializeComponent();
			cachedImages = new Dictionary<int, Bitmap>();
			loaded = false;

			exportImageTypeBox.SelectedIndex = 0;
			exportFormatBox.SelectedIndex = 0;
			SetExportEnabled(false);
			if (hideExportOptions) {
				exportLabel.Visible = false;
				exportFormatBox.Visible = false;
				exportImageTypeBox.Visible = false;
			}
		}

		private void NumberRange_Load(object sender, EventArgs e) {
			loaded = true;
			DrawPreview();
		}

		private void fromNumber_ValueChanged(object sender, EventArgs e) {
			DrawPreview();
			SetExportEnabled(FromNumber < ToNumber);
		}

		private void SetExportEnabled(bool p) {
			exportLabel.Enabled = p;
			exportFormatBox.Enabled = p && (exportImageTypeBox.SelectedIndex == 0);
			exportImageTypeBox.Enabled = p;
		}

		private void toNumber_ValueChanged(object sender, EventArgs e) {
			DrawPreview();
			SetExportEnabled(FromNumber < ToNumber);
		}

		private void DrawPreview() {
			//int direction = Math.Sign(ToNumber - FromNumber);
			//previewList.Images.Clear();
			//previewBox.Items.Clear();
			//for (int i = (int)FromNumber; i < ToNumber; i += direction) {
			//    previewList.Images.Add(Images[i].GetImage(false, false));
			//    previewBox.Items.Add(new ListViewItem(i.ToString("")));
			//}
			if (ToNumber < FromNumber || !loaded)
				return;

			int width = panel1.Width / (64 * zoom);
			if (width == 0) { width = 1; }
			canvas.Width = (64 * zoom) * width;
			canvas.Height = 64 * ((int)(ToNumber - FromNumber) / (width) + 1) * zoom;
			cachedCanvas = new Bitmap(canvas.Width, canvas.Height);

			using (Graphics g = Graphics.FromImage(cachedCanvas)) {
				dirty = false;
				int x = 0;
				int y = 0;
				g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
				g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
				for (int i = (int)FromNumber; i <= ToNumber; i++) {
					if (!cachedImages.ContainsKey(i)) {
						cachedImages.Add(i, Images[i].GetImage(false, false, false, PreviewMap));
					}
					g.DrawImage(cachedImages[i], x, y, 64 * zoom, 64 * zoom);
					x += 64 * zoom;
					if (x >= canvas.Width) {
						y += 64 * zoom;
						x = 0;
					}
				}
			}

			panel1.AutoScrollPosition = new Point(0, canvas.Height);

			canvas.Invalidate();
		}

		private void canvas_Paint(object sender, PaintEventArgs e) {
			e.Graphics.DrawImage(cachedCanvas, 0, 0);
		}

		private void NumberRange_Resize(object sender, EventArgs e) {
			DrawPreview();
		}

		private void zoomBar_ValueChanged(object sender, EventArgs e) {
			DrawPreview();
		}

		private void exportImageTypeBox_SelectedIndexChanged(object sender, EventArgs e) {
			exportFormatBox.Enabled = exportImageTypeBox.SelectedIndex == 0;
		}
	}
}
