﻿namespace WolfReColor {
	partial class GradMap {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.cancel = new System.Windows.Forms.Button();
			this.save = new System.Windows.Forms.Button();
			this.newColorLabel = new System.Windows.Forms.Label();
			this.oldColorLabel = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.newColorBox = new System.Windows.Forms.PictureBox();
			this.oldColorBox = new System.Windows.Forms.PictureBox();
			this.paletteBox = new System.Windows.Forms.PictureBox();
			this.reset = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.newColorBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.oldColorBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.paletteBox)).BeginInit();
			this.SuspendLayout();
			// 
			// cancel
			// 
			this.cancel.Location = new System.Drawing.Point(179, 432);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(75, 23);
			this.cancel.TabIndex = 15;
			this.cancel.Text = "&Cancel";
			this.cancel.UseVisualStyleBackColor = true;
			// 
			// save
			// 
			this.save.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.save.Location = new System.Drawing.Point(260, 432);
			this.save.Name = "save";
			this.save.Size = new System.Drawing.Size(75, 23);
			this.save.TabIndex = 14;
			this.save.Text = "&Save";
			this.save.UseVisualStyleBackColor = true;
			// 
			// newColorLabel
			// 
			this.newColorLabel.AutoSize = true;
			this.newColorLabel.Location = new System.Drawing.Point(9, 390);
			this.newColorLabel.Name = "newColorLabel";
			this.newColorLabel.Size = new System.Drawing.Size(32, 13);
			this.newColorLabel.TabIndex = 10;
			this.newColorLabel.Text = "New:";
			// 
			// oldColorLabel
			// 
			this.oldColorLabel.AutoSize = true;
			this.oldColorLabel.Location = new System.Drawing.Point(9, 351);
			this.oldColorLabel.Name = "oldColorLabel";
			this.oldColorLabel.Size = new System.Drawing.Size(26, 13);
			this.oldColorLabel.TabIndex = 9;
			this.oldColorLabel.Text = "Old:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(224, 13);
			this.label1.TabIndex = 16;
			this.label1.Text = "Left click to change old, right click to set new.";
			// 
			// newColorBox
			// 
			this.newColorBox.Location = new System.Drawing.Point(12, 406);
			this.newColorBox.Name = "newColorBox";
			this.newColorBox.Size = new System.Drawing.Size(320, 20);
			this.newColorBox.TabIndex = 12;
			this.newColorBox.TabStop = false;
			this.newColorBox.Paint += new System.Windows.Forms.PaintEventHandler(this.newColorBox_Paint);
			// 
			// oldColorBox
			// 
			this.oldColorBox.Location = new System.Drawing.Point(12, 367);
			this.oldColorBox.Name = "oldColorBox";
			this.oldColorBox.Size = new System.Drawing.Size(320, 20);
			this.oldColorBox.TabIndex = 11;
			this.oldColorBox.TabStop = false;
			this.oldColorBox.Paint += new System.Windows.Forms.PaintEventHandler(this.oldColorBox_Paint);
			// 
			// paletteBox
			// 
			this.paletteBox.Location = new System.Drawing.Point(12, 28);
			this.paletteBox.Name = "paletteBox";
			this.paletteBox.Size = new System.Drawing.Size(320, 320);
			this.paletteBox.TabIndex = 8;
			this.paletteBox.TabStop = false;
			this.paletteBox.Paint += new System.Windows.Forms.PaintEventHandler(this.paletteBox_Paint);
			this.paletteBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.paletteBox_MouseClick);
			// 
			// reset
			// 
			this.reset.Location = new System.Drawing.Point(12, 432);
			this.reset.Name = "reset";
			this.reset.Size = new System.Drawing.Size(75, 23);
			this.reset.TabIndex = 17;
			this.reset.Text = "&Reset";
			this.reset.UseVisualStyleBackColor = true;
			this.reset.Click += new System.EventHandler(this.reset_Click);
			// 
			// GradMap
			// 
			this.AcceptButton = this.save;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancel;
			this.ClientSize = new System.Drawing.Size(344, 463);
			this.Controls.Add(this.reset);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.save);
			this.Controls.Add(this.newColorBox);
			this.Controls.Add(this.oldColorBox);
			this.Controls.Add(this.newColorLabel);
			this.Controls.Add(this.oldColorLabel);
			this.Controls.Add(this.paletteBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "GradMap";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Gradient Map";
			((System.ComponentModel.ISupportInitialize)(this.newColorBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.oldColorBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.paletteBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Button save;
		private System.Windows.Forms.PictureBox newColorBox;
		private System.Windows.Forms.PictureBox oldColorBox;
		private System.Windows.Forms.Label newColorLabel;
		private System.Windows.Forms.Label oldColorLabel;
		private System.Windows.Forms.PictureBox paletteBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button reset;
	}
}