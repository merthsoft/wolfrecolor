﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing;

namespace WolfReColor {
	static class Extensions {
		/// <summary>
		/// Adds a filter to the FileDialog.
		/// </summary>
		/// <param name="text">The text to display.</param>
		/// <param name="ext">The extensions.</param>
		public static void AddFilter(this FileDialog ofd, string text, params string[] ext) {
			StringBuilder compiledExt = new StringBuilder(6*ext.Length);
			for (int i = 0; i < ext.Length; i++) {
				compiledExt.AppendFormat("{1}{0}", ext[i], i == 0 ? "" : ";");
			}
			ofd.Filter = string.Concat(ofd.Filter, ofd.Filter != ""?"|":"", text, "|", compiledExt.ToString());
		}

		/// <summary>
		/// Inverts this color.
		/// </summary>
		/// <returns>The color inverted.</returns>
		public static Color Invert(this Color c) {
			return Color.FromArgb(255 - c.R, 255 - c.G, 255 - c.B);
		}

		/// <summary>
		/// Read an array of elements from a <see cref="BinaryReader"/>.
		/// </summary>
		/// <typeparam name="T">The type of element to read.</typeparam>
		/// <param name="reader">The <see cref="BinaryReader"/> to read from.</param>
		/// <param name="count">The number of elements to read.</param>
		/// <returns>An array of elements of type <typeparamref name="T"/>.</returns>
		public static T[] ReadArray<T>(this BinaryReader reader, int count) {
			var rawData = reader.ReadBytes(count * Marshal.SizeOf(typeof(T)));
			var result = new T[count];
			var pinnedResult = GCHandle.Alloc(result, GCHandleType.Pinned);
			try {
				Marshal.Copy(rawData, 0, pinnedResult.AddrOfPinnedObject(), rawData.Length);
			} finally {
				pinnedResult.Free();
			}
			return result;
		}

		/// <summary>
		/// Read an array of elements from a <see cref="BinaryReader"/> into an array.
		/// </summary>
		/// <typeparam name="T">The type of element to read.</typeparam>
		/// <param name="reader">The <see cref="BinaryReader"/> to read from.</param>
		/// <param name="result">The array to read into.</param>
		/// <param name="start">Where in the array to start reading to.</param>
		/// <param name="count">The number of elements to read.</param>
		public static void ReadIntoArray<T>(this BinaryReader reader, T[] result, int start, int count) {
			var rawData = reader.ReadBytes(count * Marshal.SizeOf(typeof(T)));
			var pinnedResult = GCHandle.Alloc(result, GCHandleType.Pinned);
			try {
				Marshal.Copy(rawData, start, pinnedResult.AddrOfPinnedObject(), rawData.Length);
			} finally {
				pinnedResult.Free();
			}
		}

		/// <summary>
		/// Checks if an array is contained in this array.
		/// </summary>
		/// <typeparam name="T">The type of the lists.</typeparam>
		/// <param name="array">The array to check.</param>
		/// <param name="subarray">The subarray we're searching for.</param>
		/// <param name="index">The index of the subarray in the main array, -1 if none found.</param>
		/// <param name="length">The length of prefix match, if there is one.</param>
		/// <returns>True if the full sub-array was found in the main array.</returns>
		public static bool ContainsSubArray<T>(this IList<T> array, IList<T> subarray, out int index, out int length) {
			index = -1;
			length = 0;
			if (subarray.Count > array.Count) { return false; }
			for (index = 0; index < array.Count - subarray.Count; index++) {
				bool equal = true;
				length = 0;
				for (int j = 0; j < subarray.Count; j++) {
					if (!array[index+j].Equals(subarray[j])) {
						equal = false;
						break;
					}
					length++;
				}
				if (equal) {
					return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Converts an int into a 2-byte array.
		/// </summary>
		/// <param name="i">The int.</param>
		/// <returns>A 2-byte array representing the int casted to a short.</returns>
		public static byte[] ToByteArrayAsShort(this int i) { return BitConverter.GetBytes((short)i); }

		/// <summary>
		/// Adds an int as a short (2 bytes) to the end of this list of bytes.
		/// </summary>
		/// <param name="l">The list to add to.</param>
		/// <param name="i">The integer to add.</param>
		public static void AddIntAsShort(this List<byte> l, int i) { l.AddRange(i.ToByteArrayAsShort()); }

		/// <summary>
		/// Adds an int (4 bytes) to the end of this list of bytes.
		/// </summary>
		/// <param name="l">The list to add to.</param>
		/// <param name="i">The integer to add.</param>
		public static void AddInt(this List<byte> l, int i) { l.AddRange(BitConverter.GetBytes(i)); }
	}
}
