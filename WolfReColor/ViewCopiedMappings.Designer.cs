﻿namespace WolfReColor {
	partial class ViewCopiedMappings {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.pixelMapListBox = new WolfReColor.PixelMapListBox();
			this.SuspendLayout();
			// 
			// pixelMapListBox
			// 
			this.pixelMapListBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pixelMapListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
			this.pixelMapListBox.FormattingEnabled = true;
			this.pixelMapListBox.ItemHeight = 26;
			this.pixelMapListBox.Location = new System.Drawing.Point(0, 0);
			this.pixelMapListBox.Name = "pixelMapListBox";
			this.pixelMapListBox.Palette = null;
			this.pixelMapListBox.ScrollAlwaysVisible = true;
			this.pixelMapListBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
			this.pixelMapListBox.Size = new System.Drawing.Size(179, 408);
			this.pixelMapListBox.TabIndex = 2;
			// 
			// ViewCopiedMappings
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(179, 408);
			this.Controls.Add(this.pixelMapListBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ViewCopiedMappings";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "View Copied Mappings";
			this.ResumeLayout(false);

		}

		#endregion

		private PixelMapListBox pixelMapListBox;
	}
}