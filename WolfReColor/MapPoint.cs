﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WolfReColor {
	/// <summary>
	/// A point in the pixel map.
	/// </summary>
	public class MapPoint  {
		/// <summary>
		/// Gets or sets the X value of the point.
		/// </summary>
		public int X { get; set; }

		/// <summary>
		/// Gets or sets the Y value of the point.
		/// </summary>
		public int Y { get; set; }

		/// <summary>
		/// Gets or sets whether to exclude the pixel from the mapping or not.
		/// </summary>
		public bool Excluded { get; set; }

		/// <summary>
		/// Creates a new MapPoint object.
		/// </summary>
		/// <param name="x">The X value of the map point.</param>
		/// <param name="y">The Y value of the map point.</param>
		/// <param name="excluded">Whether or not to exclude this pixel in the mapping.</param>
		public MapPoint(int x, int y, bool excluded = false) {
			X = x;
			Y = y;
			Excluded = excluded;
		}

		/// <summary>
		/// Determines whether this MapPoint is equal to the given object. If another MapPoint 
		/// is passed in, it compares the X and Y values.
		/// </summary>
		/// <param name="obj">The object to compare with.</param>
		/// <returns>True if the two objects are the same. False otherwise.</returns>
		public override bool Equals(object obj) {
			if (obj is MapPoint) {
				MapPoint mp = (MapPoint)obj;
				return mp.X == X && mp.Y == Y;
			}
			return base.Equals(obj);
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}

		/// <summary>
		/// Returns a string representation of this MapPoint.
		/// </summary>
		/// <returns>A string representation of this MapPoint.</returns>
		/// <example>{X, Y} [Excluded]</example>
		public override string ToString() {
			return string.Format("{{{0}, {1}}} [{2}]", X, Y, Excluded);
		}
	}
}
