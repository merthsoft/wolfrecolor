﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace WolfReColor {
	public partial class Form1 : Form {
		Color[] palette;
		Vswap vswap;
		int spriteIndex = -1;
		int wallIndex = -1;
		int selectedPalette = 0;

		bool? painting = false;

		Dictionary<byte, PixelMap> copiedMaps;

		SpritePage currentSprite { get { return vswap.Sprites[spriteIndex]; } }
		WallPage currentWall { get { return vswap.Walls[wallIndex]; } }
		DataPage currentDataPage { get { return tabControl1.SelectedIndex == 0 ? (DataPage)currentWall : (DataPage)currentSprite; } }

		int zoom { get { return zoomBar.Value; } }

		public Form1() {
			InitializeComponent();

			//using (var ms = new MemoryStream(Properties.Resources.Wolfenstein3D)) {
			//List<Color> p = new List<Color>();
			//for (int i = 0; i < Properties.Resources.Wolfenstein3D.Length; i += 3) {
			//    byte red = Properties.Resources.Wolfenstein3D[i];
			//    byte green = Properties.Resources.Wolfenstein3D[i + 1];
			//    byte blue = Properties.Resources.Wolfenstein3D[i + 2];
			//    p.Add(Color.FromArgb(red, green, blue));
			//}
			//LoadPalette(p.ToArray());
			//}

			// Hehe, hacky.
			PaletteSwitch p = new PaletteSwitch();
			LoadPalette(p.Palette);
			selectedPalette = p.SelectedPalette;

			using (MemoryStream ms = new MemoryStream(Properties.Resources.pencil)) {
				wallCanvas.Cursor = new Cursor(ms);
				spriteCanvas.Cursor = wallCanvas.Cursor;
			}
		}

		private void LoadPalette(Color[] p) {
			//List<Color> p = new List<Color>();
			//for (int i = 0; i < data.Length; i += 3) {
			//    byte red = data[i];
			//    byte green = data[i + 1];
			//    byte blue = data[i + 2];
			//    p.Add(Color.FromArgb(red, green, blue));
			//}
			//palette = p.ToArray();
			palette = p;

			pixelMapListBox.Palette = palette;

			if (vswap != null) {
				vswap.Palette = palette;

				pixelMapListBox.Invalidate();
				invalidateCanvases();
			}
		}

		private void openVSAWPToolStripMenuItem_Click(object sender, EventArgs e) {
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.CheckFileExists = true;
			ofd.AddFilter("vsawp", "vsawp.*", "VSWAP.*");
			ofd.AddFilter("All files", "*.*");
			ofd.ShowDialog();
			if (!File.Exists(ofd.FileName)) {
				return;
			}

			vswap = new Vswap(ofd.FileName, palette);

			SetSprite(0);
			spriteBar.Minimum = 0;
			spriteBar.LargeChange = 10;
			spriteBar.Maximum = vswap.NumSprites + 8;

			SetWall(0);
			wallBar.Minimum = 0;
			wallBar.LargeChange = 10;
			wallBar.Maximum = vswap.NumWalls + 8;

			toolsPanel.Enabled = true;
			tabControl1.Enabled = true;
			saveToolStripMenuItem.Enabled = true;
			saveAsToolStripMenuItem.Enabled = true;
			exportToolStripMenuItem.Enabled = true;
			editToolStripMenuItem.Enabled = true;


		}

		private void invalidateCanvases() {
			spriteCanvas.Invalidate();
			wallCanvas.Invalidate();
		}

		private void spriteBar_Scroll(object sender, ScrollEventArgs e) {
			if (vswap.Sprites != null) {
				SetSprite(e.NewValue, e.Type == ScrollEventType.EndScroll);
			}
		}

		private void wallBar_Scroll(object sender, ScrollEventArgs e) {
			if (vswap.Walls != null) {
				SetWall(e.NewValue, e.Type == ScrollEventType.EndScroll);
			}
		}

		private void SetSprite(int spriteNumber, bool updatePixelMapListBox = true) {
			//SpriteBar.Value = 0;
			spriteIndex = spriteNumber;
			spriteNumberLabel.Text = string.Format("Sprite #: {0}/{1} size: {2}", spriteNumber, vswap.NumSprites - 1, currentSprite.CompressedData.Length);
			invalidateCanvases();

			pixelMapListBox.Items.Clear();
			if (updatePixelMapListBox) {
				foreach (PixelMap b in currentSprite.PixelMaps.Values) {
					pixelMapListBox.Items.Add(b);
				}
			}
		}

		private void SetWall(int wallNumber, bool updatePixelMapListBox = true) {
			//SpriteBar.Value = 0;
			wallIndex = wallNumber;
			wallNumberLabel.Text = string.Format("Wall #: {0}/{1}", wallNumber, vswap.NumWalls - 1);
			wallCanvas.Invalidate();

			pixelMapListBox.Items.Clear();
			if (updatePixelMapListBox) {
				foreach (PixelMap b in currentWall.PixelMaps.Values) {
					pixelMapListBox.Items.Add(b);
				}
			}
		}

		private void spriteCanvas_Paint(object sender, PaintEventArgs e) {
			if (spriteIndex != -1) {
				spriteCanvas.Width = 64 * zoom;
				spriteCanvas.Height = 64 * zoom;
				e.Graphics.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
				e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
				Bitmap b = currentSprite.GetImage(updateCheckBox.Checked, true, excludedPixelsCheckbox.Checked);
				e.Graphics.DrawImage(b, 0, 0, 64 * zoom, 64 * zoom);
			}
		}

		private void wallCanvas_Paint(object sender, PaintEventArgs e) {
			if (wallIndex != -1) {
				wallCanvas.Width = 64 * zoom;
				wallCanvas.Height = 64 * zoom;
				e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
				Bitmap b = currentWall.GetImage(updateCheckBox.Checked, true, excludedPixelsCheckbox.Checked);
				e.Graphics.DrawImage(b, 0, 0, 64 * zoom, 64 * zoom);
			}
		}

		private void highlightPixelsToolStripMenuItem_Click(object sender, EventArgs e) {
			PixelMap item = pixelMapListBox.SelectedItem as PixelMap;
			if (item == null) { return; }
			currentDataPage.HighlightedPixel = currentDataPage.HighlightedPixel == item.OldColor ? null : (byte?)item.OldColor;
			invalidateCanvases();
		}

		private void pixelMapListBox_MouseDoubleClick(object sender, MouseEventArgs e) {
			PixelMap pm = (PixelMap)pixelMapListBox.SelectedItem;
			PaletteChoice pc = new PaletteChoice(palette, pm.OldColor, pm.NewColor);
			pc.ShowDialog();
			pm.NewColor = pc.NewColor;
			invalidateCanvases();
		}

		private void updateCheckBox_CheckedChanged(object sender, EventArgs e) {
			invalidateCanvases();
		}

		private void excludedPixelsCheckbox_CheckedChanged(object sender, EventArgs e) {
			invalidateCanvases();
		}

		private void copyMappingButton_Click(object sender, EventArgs e) {
			CopyMapping(true);
		}

		private void CopyMapping(bool copyInto) {
			if (!copyInto || copiedMaps == null) {
				copiedMaps = new Dictionary<byte, PixelMap>();
			}

			foreach (PixelMap p in currentDataPage.PixelMaps.Values) {
				if (p.OldColor == p.NewColor) { continue; }
				if (copiedMaps.ContainsKey(p.OldColor)) {
					copiedMaps[p.OldColor] = new PixelMap(p.OldColor, p.NewColor);
				} else {
					copiedMaps.Add(p.OldColor, new PixelMap(p.OldColor, p.NewColor));
				}
			}
		}

		private void pasteMappingButton_Click(object sender, EventArgs e) {
			PasteMapping(currentDataPage);
		}

		private void PasteMapping(DataPage dp) {
			foreach (PixelMap p in copiedMaps.Values) {
				if (dp.PixelMaps.ContainsKey(p.OldColor)) {
					PixelMap pm = dp.PixelMaps[p.OldColor];
					//if (pm.NewColor == pm.NewColor || !pasteInto) {
					pm.NewColor = p.NewColor;
					//}
				}
			}
			invalidateCanvases();
			pixelMapListBox.Invalidate();
		}

		private void resetMappingButton_Click(object sender, EventArgs e) {
			foreach (PixelMap p in currentDataPage.PixelMaps.Values) {
				p.NewColor = p.OldColor;
			}
			invalidateCanvases();
			pixelMapListBox.Invalidate();
		}

		private void saveMappingButton_Click(object sender, EventArgs e) {
			currentDataPage.CommitPixelMaps();
			SetDataPage();
		}

		private void loadPaletteToolStripMenuItem_Click(object sender, EventArgs e) {
			//OpenFileDialog ofd = new OpenFileDialog();
			//ofd.AddFilter("Palette", "*.PAL", "*.pal");
			//ofd.ShowDialog();
			//if (!File.Exists(ofd.FileName)) {
			//    return;
			//}

			//if (ofd.FileName != "") {
			//    LoadPalette(new FileStream(ofd.FileName, FileMode.Open));
			//}
			PaletteSwitch p = new PaletteSwitch();
			p.SelectedPalette = selectedPalette;
			if (p.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				LoadPalette(p.Palette);
				selectedPalette = p.SelectedPalette;
			}
		}

		private void exportToolStripMenuItem_Click(object sender, EventArgs e) {
			int to;
			int from;
			int zoom;
			int ext;
			int exportType;
			if (!GetNumberRange(out from, out to, false, false, out ext, out exportType, out zoom)) { return; }

			if (from == to) {
				SaveFileDialog sfd = new SaveFileDialog();
				sfd.AddFilter("Bitmap", "*.bmp");
				sfd.AddFilter("Png", "*.png");
				if (sfd.ShowDialog() != System.Windows.Forms.DialogResult.OK) {
					return;
				}

				SaveImage(from, zoom, sfd.FileName);
			} else {
				if (exportType == 0) { // Multi
					FolderBrowserDialog fbd = new FolderBrowserDialog();
					if (fbd.ShowDialog() != System.Windows.Forms.DialogResult.OK || !Directory.Exists(fbd.SelectedPath)) {
						return;
					}

					for (int i = from; i <= to; i++) {
						string filename = Path.Combine(fbd.SelectedPath, i.ToString() + (ext==0?".bmp":".png"));
						SaveImage(i, zoom, filename);
					}
				} else if (exportType == 1 || exportType == 2) { // Long or tall
					SaveFileDialog sfd = new SaveFileDialog();
					sfd.AddFilter("Bitmap", "*.bmp");
					sfd.AddFilter("Png", "*.png");
					if (sfd.ShowDialog() != System.Windows.Forms.DialogResult.OK) {
						return;
					}

					Bitmap b;
					if (exportType == 1) {
						b = new Bitmap(64 * zoom * (to - from + 1), 64 * zoom);
					} else {
						b = new Bitmap(64 * zoom, 64 * zoom * (to - from + 1));
					}

					using (Graphics g = Graphics.FromImage(b)) {
						for (int imageNumber = from; imageNumber <= to; imageNumber++) {
							DataPage page = (currentDataPage is SpritePage) ? (DataPage)vswap.Sprites[imageNumber] : vswap.Walls[imageNumber];
							g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
							g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
							if (exportType == 1) {
								g.DrawImage(page.GetImage(false, false, false), 64 * zoom * imageNumber, 0, 64 * zoom, 64 * zoom);
							} else {
								g.DrawImage(page.GetImage(false, false, false), 0, 64 * zoom * imageNumber, 64 * zoom, 64 * zoom);
							}
						}
					}

					b.Save(sfd.FileName,
						new FileInfo(sfd.FileName).Extension.EndsWith("png", StringComparison.CurrentCultureIgnoreCase) ? ImageFormat.Png : ImageFormat.Bmp);
				} else { // Square
					SaveFileDialog sfd = new SaveFileDialog();
					sfd.AddFilter("Bitmap", "*.bmp");
					sfd.AddFilter("Png", "*.png");
					if (sfd.ShowDialog() != System.Windows.Forms.DialogResult.OK) {
						return;
					}

					int num = to - from + 1;
					int width = (int)Math.Ceiling(Math.Sqrt(num));
					int height = width - (width * width > num ? 1 : 0);

					Bitmap b = new Bitmap(width * 64 * zoom, height * 64 * zoom);

					using (Graphics g = Graphics.FromImage(b)) {
						g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
						g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;

						int imageNumber = from;
						for (int j = 0; j < height; j++) {
							for (int i = 0; i < width; i++) {
								if (imageNumber > to) { break; }
								DataPage page = (currentDataPage is SpritePage) ? (DataPage)vswap.Sprites[imageNumber] : vswap.Walls[imageNumber];
								g.DrawImage(page.GetImage(false, false, false), i * 64 * zoom, j * 64 * zoom, 64 * zoom, 64 * zoom);
								imageNumber++;
							}
							if (imageNumber > to) { break; }
						}

					}

					b.Save(sfd.FileName, 
						new FileInfo(sfd.FileName).Extension.EndsWith("png", StringComparison.CurrentCultureIgnoreCase)?ImageFormat.Png:ImageFormat.Bmp);
				}
			}
		}

		private void SaveImage(int imageNumber, int zoom, string filename) {
			DataPage page = (currentDataPage is SpritePage) ? (DataPage)vswap.Sprites[imageNumber] : vswap.Walls[imageNumber];
			Bitmap b = new Bitmap(64 * zoom, 64 * zoom);
			using (Graphics g = Graphics.FromImage(b)) {
				g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
				g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
				g.DrawImage(page.GetImage(false, false, false), 0, 0, 64 * zoom, 64 * zoom);
			}

			b.Save(filename,
						new FileInfo(filename).Extension.EndsWith("png", StringComparison.CurrentCultureIgnoreCase) ? ImageFormat.Png : ImageFormat.Bmp);
		}

		private void pasteMappingAcrossToolStripMenuItem1_Click(object sender, EventArgs e) {
			int to;
			int from;
			int dummyInt;
			DataPage[] pages;
			if (!GetNumberRange(out from, out to, true, true, out dummyInt, out dummyInt, out dummyInt)) { return; }

			if (currentDataPage is SpritePage) {
				pages = vswap.Sprites.GetRange(from, to - from + 1).ToArray();
			} else {
				pages = vswap.Walls.GetRange(from, to - from + 1).ToArray();
			}

			Array.ForEach(pages, dp => PasteMapping(dp));
		}

		private bool GetNumberRange(out int from, out int to, bool useMapping, bool hideExportOptions, out int ext, out int single, out int zoom) {
			NumberRange nr = new NumberRange(hideExportOptions);
			nr.Minimum = 0;
			if (currentDataPage is SpritePage) {
				nr.Maximum = vswap.NumSprites - 1;
				nr.Images = vswap.Sprites.ToArray();
				nr.FromNumber = spriteIndex;
				nr.ToNumber = spriteIndex;
			} else {
				nr.Maximum = vswap.NumWalls - 1;
				nr.Images = vswap.Walls.ToArray();
				nr.FromNumber = wallIndex;
				nr.ToNumber = wallIndex;
			}
			if (useMapping) { nr.PreviewMap = copiedMaps; }
			var res = nr.ShowDialog();
			to = (int)nr.ToNumber;
			from = (int)nr.FromNumber;
			ext = nr.ExportExtension;
			single = nr.ExportImageType;
			zoom = nr.Zoom;

			return res == System.Windows.Forms.DialogResult.OK && to >= from;
		}

		private void overwriteClipboardToolStripMenuItem_Click(object sender, EventArgs e) {
			CopyMapping(false);
		}

		private void tabControl1_SelectedIndexChanged(object sender, EventArgs e) {
			if (vswap == null) { return; }
			SetDataPage();
		}

		private void SetDataPage() {
			if (currentDataPage is SpritePage) {
				SetSprite(spriteIndex);
			} else {
				SetWall(wallIndex);
			}
		}

		private void saveVSWAPToolStripMenuItem_Click(object sender, EventArgs e) {
			vswap.Save();
		}

		private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
			new AboutBox1().ShowDialog();
		}

		private void saveVswapAsToolStripMenuItem_Click(object sender, EventArgs e) {
			SaveFileDialog sfd = new SaveFileDialog();
			FileInfo fi = new FileInfo(vswap.FileName);

			sfd.InitialDirectory = fi.DirectoryName;
			sfd.FileName = fi.Name;
			sfd.CheckFileExists = true;
			sfd.ShowDialog();
			if (File.Exists(sfd.FileName)) {
				return;
			}

			vswap.Save(sfd.FileName);
		}

		private void zoomBar_Scroll(object sender, EventArgs e) {
			//zoom = zoomBar.Value;
			invalidateCanvases();
		}

		private void canvas_MouseDown(object sender, MouseEventArgs e) {
			painting = e.Button == System.Windows.Forms.MouseButtons.Left;
			int x = e.X / zoom;
			int y = e.Y / zoom;
			foreach (PixelMap pm in currentDataPage.PixelMaps.Values) {
				foreach (MapPoint mp in pm.Points) {
					if (mp.X == x && mp.Y == y) {
						mp.Excluded = painting.Value;
						invalidateCanvases();
						break;
					}
				}
			}
		}

		private void canvas_MouseMove(object sender, MouseEventArgs e) {
			if (painting != null) {
				int x = e.X / zoom;
				int y = e.Y / zoom;
				foreach (PixelMap pm in currentDataPage.PixelMaps.Values) {
					foreach (MapPoint mp in pm.Points) {
						if (mp.X == x && mp.Y == y) {
							mp.Excluded = painting.Value;
							invalidateCanvases();
							break;
						}
					}
				}
			}
		}

		private void canvas_MouseUp(object sender, MouseEventArgs e) {
			painting = null;
		}

		private void clearClipbaordToolStripMenuItem_Click(object sender, EventArgs e) {
			copiedMaps.Clear();
		}

		private void viewMappingtoolStripMenuItem_Click(object sender, EventArgs e) {
			ViewCopiedMappings vcm = new ViewCopiedMappings();
			vcm.Palette = palette;
			vcm.CopiedMaps = copiedMaps;
			vcm.ShowDialog();
		}

		private void gradietMapToolStripMenuItem_Click(object sender, EventArgs e) {
			GradMap gm = new GradMap();
			gm.Palette = palette;
			if (gm.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				if (copiedMaps == null) {
					copiedMaps = new Dictionary<byte, PixelMap>();
				}
				foreach (PixelMap pm in gm.Gradient) {
					if (copiedMaps.ContainsKey(pm.OldColor)) {
						copiedMaps[pm.OldColor].NewColor = pm.NewColor;
					} else {
						copiedMaps.Add(pm.OldColor, pm);
					}
					if (currentDataPage.PixelMaps.ContainsKey(pm.OldColor)) {
						currentDataPage.PixelMaps[pm.OldColor].NewColor = pm.NewColor;
					}
				}
				invalidateCanvases();
				pixelMapListBox.Invalidate();
			}
		}
	}
}
