﻿using System;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace WolfReColor {
	/// <summary>
	/// A sprite in the vswap.
	/// </summary>
	class SpritePage : DataPage {
		/// <summary>
		/// A contiguous section of pixels in a column.
		/// </summary>
		private class ColumnBit : IComparable<ColumnBit> {
			/// <summary>
			/// Gets or sets the top pixel of this ColumnBit.
			/// </summary>
			public int Top { get; set; }

			/// <summary>
			/// Gets or sets the bottom pixel of this ColumnBit.
			/// </summary>
			public int Bottom { get; set; }

			/// <summary>
			/// Gets or sets the pixel data of this ColumnBit.
			/// </summary>
			public List<byte> Data { get; set; }

			/// <summary>
			/// Initializes a new instance of a ColumnBit.
			/// </summary>
			/// <param name="top">The top pixel of this ColumnBit.</param>
			public ColumnBit(int top) {
				Top = top;
				Data = new List<byte>();
			}

			/// <summary>
			/// Compares two ColumnBits based on their data length.
			/// </summary>
			/// <param name="other">The other ColumnBit to compare to.</param>
			/// <returns>The integer comparison of the two ColumnBits' data length.</returns>
			public int CompareTo(ColumnBit other) {
				return Data.Count.CompareTo(other.Data.Count);
			}
		}

		/// <summary>
		/// Creates a new SpritePage from the given BinaryReader, using the given palette.
		/// </summary>
		/// <param name="br">The binary reader to read data from.</param>
		/// <param name="size">The size of the entire data.</param>
		/// <param name="palette">The palette.</param>
		public SpritePage(BinaryReader br, int size, Color[] palette)
			: base(palette) {
			// The data is leftCol,rightCol,data[]
			short leftmostColumn = br.ReadInt16();
			short rightmostColumn = br.ReadInt16();
			byte[] data = br.ReadArray<byte>(size-4);

			// Start at the leftmost column and go to the rightmost column
			for (int i = leftmostColumn; i <= rightmostColumn; i++) {
				// The data location for this column is stored based on which column we're looking at:
				// so i-leftmostColumn gives us the offset into the array, multiply by two because
				// the array is bytes, but the location is a short.
				int dataLocation = ReadShort(data, (i - leftmostColumn) * 2);
				// Subtract 4 because the data location is stored in relation to the VERY start
				// of data, which includes the four byte header.
				dataLocation -= 4;
				// This will loop through all the bits in this column
				while (true) {
					// Get the bottom pixel location of this bit of this column.
					// I don't know why it's stored times 2.
					int bottomOfColumn = ReadShort(data, dataLocation) / 2;
					// If it's zero, we're at the end.
					if (bottomOfColumn == 0) {
						break;
					}
					// After the bottom pixel is where in the array the data starts, minus 4 for the header (see above)
					int dataStart = ReadShort(data, dataLocation + 2) - 4;
					// The pixel location of the top of this bit of this column
					// I don't know why it's stored times 2.
					int topOfColumn = ReadShort(data, dataLocation + 4) / 2;
					// Seek to the right section of the data after we load the meta-data in
					dataLocation += 6;
					// Loop over the data, setting the sprite info into the array
					for (int j = topOfColumn; j < bottomOfColumn; j++) {
						byte color = data[dataStart + j];
						if (!PixelMaps.ContainsKey(color)) {
							PixelMaps.Add(color, new PixelMap(color));
						}
						MapPoint mp = new MapPoint(i, j);
						PixelMaps[color].Points.Add(mp);
					}
				}
			}
		}

		/// <summary>
		/// Reads a short from from the data array at the given position.
		/// </summary>
		/// <param name="data">The data to read from.</param>
		/// <param name="position">The position in the data to read from.</param>
		/// <returns></returns>
		private short ReadShort(byte[] data, int position) {
			short val = (short)(data[position] | data[position + 1] << 8);
			return val;
		}

		/// <summary>
		/// Gets the data for the sprite compressed
		/// </summary>
		public override byte[] CompressedData {
			get {
				// Stores the leftmost and rightmost columns, as well as where the metaj data is
				List<byte> metai = new List<byte>();
				// Stores the top pixel, data location, and bottom pixel for each column bit
				List<byte> metaj = new List<byte>();
				// Stores the actual data.
				List<byte> data = new List<byte>();
				byte[,] imageData = ImageData;
				List<ColumnBit>[] cols = new List<ColumnBit>[64];
				cols.Initialize();

				int leftMost = -1;
				int rightMost = -1;
				int numColumnBits = 0;

				// Get the left most and right most columns, as well as populate
				// the list of columnbits we're going to use.
				ColumnBit currentBit = null;
				for (int i = 0; i < 64; i++) {
					cols[i] = new List<ColumnBit>();

					// This happens if we got to the bottom of the column
					if (currentBit != null) {
						currentBit.Bottom = 64;
						cols[i - 1].Add(currentBit);
					}
					currentBit = null;

					for (int j = 0; j < 64; j++) {
						// If there's a pixel here, we put the information into the column bit (creating a new on if there isn't one)
						// If there's no pixel and a ColumnBit, close the column bit.
						if (imageData[j, i] != 255) {
							if (currentBit == null) { currentBit = new ColumnBit(j); numColumnBits++; }
							currentBit.Data.Add(imageData[j, i]);

							if (leftMost == -1) { leftMost = i; }
							rightMost = i;
						} else {
							if (currentBit != null) {
								currentBit.Bottom = j;
								cols[i].Add(currentBit);
								currentBit = null;
							}
						}
					}
				}

				// If it's an empty sprite, skip all the work.
				if (leftMost == -1) {
					return new byte[] { 0, 0, 0, 0, 6, 0, 0, 0 };
				}
				
				metai.AddIntAsShort(leftMost);
				metai.AddIntAsShort(rightMost);

				int metaiLen = 2 * (rightMost - leftMost + 1) + 4;
				int metajLen = 2 * (rightMost - leftMost + 1) + 6 * (numColumnBits);
				int dataPointer = 0;
				int jPointer = 0;

				for (int i = leftMost; i <= rightMost; i++) {
					List<ColumnBit> column = cols[i];
					// Sort big data to small to hopefully maximize overlap.
					column.Sort();

					// Add the location of the metaj information
					metai.AddIntAsShort(jPointer + metaiLen);
					// Store each column bit.
					column.ForEach(cb => StoreColumnBit(cb, metaj, data, metaiLen, metajLen, ref dataPointer, ref jPointer));
					
					// Null-terminated
					metaj.AddIntAsShort(0);
					jPointer += 2;
				}

				metai.AddRange(metaj);
				metai.AddRange(data);
				return metai.ToArray();
			}
		}

		/// <summary>
		/// Store the column bit to the data.
		/// </summary>
		/// <param name="cb">The column bit.</param>
		/// <param name="metaj">The metaj data to store to.</param>
		/// <param name="data">The data array to check in and store to if there's no overlap.</param>
		/// <param name="metaiLen">The length of the metai section.</param>
		/// <param name="metajLen">The length of the metaj section.</param>
		/// <param name="dataPointer">The current bottom of the data array.</param>
		/// <param name="jPointer">The current bottom of the metaj array.</param>
		private static void StoreColumnBit(ColumnBit cb, List<byte> metaj, List<byte> data, int metaiLen, int metajLen, ref int dataPointer, ref int jPointer) {
			int storeLocation;
			int length;
			//ColumnBit restOfColumn = null;

			// Put the bottom pixel into the metaj data.
			metaj.AddIntAsShort(cb.Bottom * 2);
			// Check for overlap, and it it exists, point there instead of adding new data.
            if (data.ContainsSubArray(cb.Data, out storeLocation, out length)) {
				metaj.AddIntAsShort(storeLocation + metaiLen + metajLen - cb.Top);
			//} else if (length > 6) {
			//    restOfColumn = new ColumnBit(cb.Bottom - length+1);
			//    restOfColumn.Data = cb.Data.GetRange(length, cb.Data.Count - length);
			//    restOfColumn.Bottom = cb.Bottom;
			//    metaj.RemoveRange(metaj.Count - 2, 2);
			//    metaj.AddIntAsShort(cb.Bottom - length);
			//    metaj.AddIntAsShort(storeLocation + metaiLen + metajLen - cb.Top);
			} else if (metaj.ContainsSubArray(cb.Data, out storeLocation, out length)) {
				metaj.AddIntAsShort(metaiLen + storeLocation - cb.Top);
			//} else if (metai.ContainsSubArray(cb.Data, out storeLocation, out length)) {
			//	metaj.AddIntAsShort(storeLocation - cb.Top);
			} else {
				// There's no overlap, so add new data :(
				metaj.AddIntAsShort(dataPointer + metaiLen + metajLen - cb.Top);
				data.AddRange(cb.Data);
				dataPointer += cb.Data.Count;
			}
			// Store the top.
			metaj.AddIntAsShort(cb.Top * 2);
			jPointer += 6;
			//Console.WriteLine(cb.Bottom - cb.Top - cb.Data.Count);

			//if (restOfColumn != null) {
			//    StoreColumnBit(restOfColumn, metaj, data, metaiLen, metajLen, ref dataPointer, ref jPointer);
			//}
		}
	}
}
