﻿namespace WolfReColor {
	partial class GraphicsControl {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.panel3 = new System.Windows.Forms.Panel();
			this.canvas = new System.Windows.Forms.PictureBox();
			this.numberLabel = new System.Windows.Forms.Label();
			this.scrollBar = new System.Windows.Forms.HScrollBar();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.canvas)).BeginInit();
			this.SuspendLayout();
			// 
			// panel3
			// 
			this.panel3.AutoScroll = true;
			this.panel3.BackColor = System.Drawing.Color.Transparent;
			this.panel3.Controls.Add(this.canvas);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(0, 13);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(490, 476);
			this.panel3.TabIndex = 8;
			// 
			// canvas
			// 
			this.canvas.Cursor = System.Windows.Forms.Cursors.Arrow;
			this.canvas.Location = new System.Drawing.Point(0, 0);
			this.canvas.Name = "canvas";
			this.canvas.Size = new System.Drawing.Size(219, 177);
			this.canvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.canvas.TabIndex = 4;
			this.canvas.TabStop = false;
			// 
			// numberLabel
			// 
			this.numberLabel.AutoSize = true;
			this.numberLabel.Dock = System.Windows.Forms.DockStyle.Top;
			this.numberLabel.Location = new System.Drawing.Point(0, 0);
			this.numberLabel.Name = "numberLabel";
			this.numberLabel.Size = new System.Drawing.Size(61, 13);
			this.numberLabel.TabIndex = 7;
			this.numberLabel.Text = "XXX #: 0/0";
			this.numberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// scrollBar
			// 
			this.scrollBar.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.scrollBar.LargeChange = 1;
			this.scrollBar.Location = new System.Drawing.Point(0, 489);
			this.scrollBar.Maximum = 0;
			this.scrollBar.Name = "scrollBar";
			this.scrollBar.Size = new System.Drawing.Size(490, 17);
			this.scrollBar.TabIndex = 6;
			this.scrollBar.TabStop = true;
			// 
			// GraphicsControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Transparent;
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.numberLabel);
			this.Controls.Add(this.scrollBar);
			this.Name = "GraphicsControl";
			this.Size = new System.Drawing.Size(490, 506);
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.canvas)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.PictureBox canvas;
		private System.Windows.Forms.Label numberLabel;
		private System.Windows.Forms.HScrollBar scrollBar;
	}
}
