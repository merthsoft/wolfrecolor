﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;

namespace WolfReColor {
	/// <summary>
	/// Represents a vswap.
	/// </summary>
	class Vswap {
		/// <summary>
		/// Gets or sets the list of SpritePages in the vswap.
		/// </summary>
		public List<SpritePage> Sprites { get; set; }

		/// <summary>
		/// Gets or sets the list of WallPages in the vswap.
		/// </summary>
		public List<WallPage> Walls { get; set; }

		/// <summary>
		/// Gets or sets the list of SoundPages in the vswap.
		/// </summary>
		public List<SoundPage> Sounds { get; set; }

		/// <summary>
		/// Palette of colors.
		/// </summary>
		private Color[] palette;

		/// <summary>
		/// Gets of sets the array of colors that represent the palette.
		/// </summary>
		public Color[] Palette {
			get {
				return palette;
			}
			set {
				palette = value;
				if (Sprites != null) {
					foreach (SpritePage sp in Sprites) {
						sp.Palette = palette;
					}
					foreach (WallPage wp in Walls) {
						wp.Palette = palette;
					}
				}
			}
		}

		/// <summary>
		/// Gets the number of sprites in the vswap.
		/// </summary>
		public int NumSprites { get { return Sprites.Count; } }

		/// <summary>
		/// Gets the number of walls in the vswap.
		/// </summary>
		public int NumWalls { get { return Walls.Count; } }

		/// <summary>
		/// Gets the number of sounds in the vswap.
		/// </summary>
		public int NumSounds { get { return Sounds.Count; } }

		/// <summary>
		/// Gets or sets the filename to save to.
		/// </summary>
		public string FileName { get; set; }

		/// <summary>
		/// Creates a new Vswap object by loading in a vswap file.
		/// </summary>
		/// <param name="filename">The vswap file to load.</param>
		/// <param name="palette">The array of colors that represent the palette</param>
		public Vswap(string filename, Color[] palette) {
			FileName = filename;
			Palette = palette;

			int chunksInFile = 0;
			int spriteStart = 0;
			int soundStart = 0;
			int[] pageOffsets;
			short[] pageLengths;
			int pageDataSize = 0;
			uint[] pageData;

			SpritePage[] sprites;
			WallPage[] walls;
			SoundPage[] sounds;

			using (BinaryReader br = new BinaryReader(new FileStream(filename, FileMode.Open))) {
				chunksInFile = br.ReadInt16();
				spriteStart = br.ReadInt16();
				// The start of sound is the end of sprites
				soundStart = br.ReadInt16();
				pageOffsets = new int[chunksInFile + 1];
				br.ReadIntoArray<int>(pageOffsets, 0, chunksInFile);
				pageOffsets[chunksInFile] = (int)br.BaseStream.Length;
				pageLengths = br.ReadArray<short>(chunksInFile);

				//using (StreamWriter sw = new StreamWriter(new FileStream("dump.txt", FileMode.Append))) {
				//    sw.WriteLine("---------------------------------------------------------------");
				//    for (int i = 0; i < soundStart; i++) {
				//        short meta = (short)(pageLengths[i] >> 16);
				//        short len = (short)(pageLengths[i] & 0x0000FFFF);
				//        sw.WriteLine(string.Format("{2}: {0:X4} - {1:X4}", meta, len, i.ToString().PadLeft(3)));
				//    }
				//}

				int dataStart = pageOffsets[0];

				// Check that all pageOffsets are valid
				for (int i = 0; i < chunksInFile; i++) {
					if (pageOffsets[i] == 0)
						continue;   // sparse page
					if (pageOffsets[i] < dataStart || pageOffsets[i] >= br.BaseStream.Length) {
						//MessageBox.Show(string.Format("Illegal page offset for page {0}: {1} (file size: {2})",
						//		i, pageOffsets[i], br.BaseStream.Length));
						throw new Exception(string.Format("Illegal page offset for page {0}: {1} (file size: {2})",
								i, pageOffsets[i], br.BaseStream.Length));
						//return;
					}
				}

				// Calculate total amount of padding needed for sprites and sound info page
				int alignPadding = 0;
				for (int i = 0; i < soundStart; i++) {
					if (pageOffsets[i] == 0)
						continue;   // sparse page
					int offs = pageOffsets[i] - dataStart + alignPadding;
					if ((offs & 1) == 1)
						alignPadding++;
				}

				if (((pageOffsets[chunksInFile - 1] - dataStart + alignPadding) & 1) == 1)
					alignPadding++;

				pageDataSize = (int)br.BaseStream.Length - pageOffsets[0] + alignPadding;
				pageData = new uint[pageDataSize];

				sprites = new SpritePage[soundStart - spriteStart];
				walls = new WallPage[spriteStart];
				sounds = new SoundPage[chunksInFile - soundStart];

				// Load pages and initialize PMPages pointers
				uint ptr = pageData[0];
				for (int i = 0; i < chunksInFile; i++) {
					//if (i >= spriteStart && i < soundStart || i == chunksInFile - 1) {
					//    size_t offs = ptr - (uint8_t*)PMPageData;

					//    // pad with zeros to make it 2-byte aligned
					//    if (offs & 1) {
					//        *ptr++ = 0;
					//        if (i == ChunksInFile - 1)
					//            PMSoundInfoPagePadded = true;
					//    }
					//}

					//pages[i] = ptr;

					if (pageOffsets[i] == 0) {
						continue;               // sparse page
					}

					// Use specified page length, when next page is sparse page.
					// Otherwise, calculate size from the offset difference between this and the next page.
					int size = pageOffsets[i + 1] == 0 ? pageLengths[i] : pageOffsets[i + 1] - pageOffsets[i];

					br.BaseStream.Seek(pageOffsets[i], SeekOrigin.Begin);
					if (i < spriteStart) {
						walls[i] = new WallPage(br, size, palette);
					} else if (i < soundStart) {
						sprites[i - spriteStart] = new SpritePage(br, size, palette);
					} else {
						sounds[i - soundStart] = new SoundPage(br, size);
					}
				}
			}

			Sprites = new List<SpritePage>(sprites);
			Walls = new List<WallPage>(walls);
			Sounds = new List<SoundPage>(sounds);
		}

		/// <summary>
		/// Saves the vswap file. If a filename is passed in, saves it to that file.
		/// Otherwise it saves it to the file it was loaded from or last saved to.
		/// </summary>
		/// <param name="filename">The file to save to.</param>
		public void Save(string filename = null) {
			if (!string.IsNullOrEmpty(filename)) {
				FileName = filename;
			}
			
			int chunksInFile = NumWalls + NumSprites + NumSounds;
			int spriteStart = NumWalls;
			int soundStart = NumWalls + NumSprites;

			List<byte> meta = new List<byte>();
			List<byte> pageOffsets = new List<byte>();
			List<byte> pageLengths = new List<byte>();
			List<byte> data = new List<byte>();

			meta.AddIntAsShort(chunksInFile);
			meta.AddIntAsShort(spriteStart);
			meta.AddIntAsShort(soundStart);

			int dataPointer = 6 + 6 * chunksInFile;
			// Save each item in order. First all the walls, then all the sprites, then all the sounds.
			Walls.ForEach(d => SaveDataChunk(pageOffsets, pageLengths, data, ref dataPointer, d.CompressedData));
			Sprites.ForEach(d => SaveDataChunk(pageOffsets, pageLengths, data, ref dataPointer, d.CompressedData));
			Sounds.ForEach(d => SaveDataChunk(pageOffsets, pageLengths, data, ref dataPointer, d.CompressedData));

			using (BinaryWriter bw = new BinaryWriter(new FileStream(FileName, FileMode.Create))) {
				meta.ForEach(b => bw.Write(b));
				pageOffsets.ForEach(b => bw.Write(b));
				pageLengths.ForEach(b => bw.Write(b));
				data.ForEach(b => bw.Write(b));
			}
		}

		/// <summary>
		/// Saves a data chunk (like a spite or a wall), and updates page offsets, page lengths, and data points.
		/// </summary>
		/// <param name="pageOffsets">The list of page offsets.</param>
		/// <param name="pageLengths">The list of page lengths.</param>
		/// <param name="data">The data to write to.</param>
		/// <param name="dataPointer">Where the data is currently pointing to.</param>
		/// <param name="dataChunk">The chunk of data to save.</param>
		private static void SaveDataChunk(List<byte> pageOffsets, List<byte> pageLengths, List<byte> data, ref int dataPointer, byte[] dataChunk) {
			pageOffsets.AddInt(dataPointer);
			pageLengths.AddIntAsShort(dataChunk.Length);
			data.AddRange(dataChunk);
			dataPointer += dataChunk.Length;
		}
	}
}
