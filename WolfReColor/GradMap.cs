﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WolfReColor {
	public partial class GradMap : Form {
		/// <summary>
		/// Gets or sets the palette.
		/// </summary>
		public Color[] Palette { get; set; }

		bool clickedYet = false;
		byte oldColorStart;
		byte oldColorEnd;
		byte newColorStart;
		byte newColorEnd { get { return (byte)(newColorStart + (byte)NumColors); } }

		private int NumColors {
			get {
				return oldColorEnd - oldColorStart;
			}
		}

		/// <summary>
		/// Gets the gradient.
		/// </summary>
		public List<PixelMap> Gradient {
			get {
				List<PixelMap> ret = new List<PixelMap>(NumColors);
				for (int i = 0; i < NumColors+1; i++) {
					ret.Add(new PixelMap((byte)(i + oldColorStart), (byte)(i + newColorStart)));
				}

				return ret;
			}
		}

		public GradMap() {
			InitializeComponent();
		}

		private void paletteBox_Paint(object sender, PaintEventArgs e) {
			int x = 0;
			int y = 0;
			for (int i = 0; i < Palette.Length; i++) {
				Color c = Palette[i];
				e.Graphics.FillRectangle(new SolidBrush(c), x, y, 20, 20);
				if (clickedYet) {
					if (i == oldColorStart || i == oldColorEnd) {
						e.Graphics.DrawString("O", DefaultFont, new SolidBrush(c.Invert()), x + 5, y + 5);
					} else if (i == newColorStart || i == newColorEnd) {
						e.Graphics.DrawString("N", DefaultFont, new SolidBrush(c.Invert()), x + 5, y + 5);
					}
				}

				x += 20;
				if (x >= e.ClipRectangle.Width) {
					y += 20;
					x = 0;
				}
			}
		}

		private void paletteBox_MouseClick(object sender, MouseEventArgs e) {
			byte clicked = (byte)(e.X / 20 + 16 * (e.Y / 20));
			if (e.Button == System.Windows.Forms.MouseButtons.Left) {
				if (!clickedYet) {
					oldColorEnd = clicked;
					oldColorStart = clicked;
					newColorStart = clicked;
					clickedYet = true;
				} else {
					if (clicked < oldColorStart) {
						if (newColorStart + (oldColorEnd - clicked) > 256) { return; }
						oldColorStart = clicked;
					} else {
						if (newColorStart + (clicked - oldColorStart) > 256) { return; }
						oldColorEnd = clicked;
					}
				}
			} else if (e.Button == System.Windows.Forms.MouseButtons.Right) {
				if (clicked + NumColors > 256) { return; }
				if (clickedYet) { newColorStart = clicked; }
			}

			oldColorLabel.Text = string.Format("Old: {0:X2} - {1:X2}", oldColorStart, oldColorEnd);
			newColorLabel.Text = string.Format("New: {0:X2} - {1:X2}", newColorStart, newColorEnd);
			InvalidateCanvases();
		}

		private void InvalidateCanvases() {
			paletteBox.Invalidate();
			oldColorBox.Invalidate();
			newColorBox.Invalidate();
		}

		private void oldColorBox_Paint(object sender, PaintEventArgs e) {
			if (!clickedYet) { return; }
			int width = NumColors > 0 ? 320 / (NumColors+1) : 320;
			for (int i = 0; i <= NumColors; i++) {
				e.Graphics.FillRectangle(new SolidBrush(Palette[i + oldColorStart]), i * width, 0, width, 20);
			}
		}

		private void newColorBox_Paint(object sender, PaintEventArgs e) {
			if (!clickedYet) { return; }
			int width = NumColors > 0 ? 320 / (NumColors+1) : 320;
			for (int i = 0; i <= NumColors; i++) {
				e.Graphics.FillRectangle(new SolidBrush(Palette[i + newColorStart]), i * width, 0, width, 20);
			}
		}

		private void reset_Click(object sender, EventArgs e) {
			clickedYet = false;
			oldColorLabel.Text = "Old:";
			newColorLabel.Text = "New:";
			InvalidateCanvases();
		}
	}
}
