﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WolfReColor {
	public partial class PaletteChoice : Form {
		Color[] palette;
		byte oldColor;
		public byte NewColor { get; private set; }
		byte originalNewColor;

		public PaletteChoice(Color[] palette, byte originalColor, byte newColor) {
			this.palette = palette;
			this.oldColor = originalColor;
			this.NewColor = newColor;
			originalNewColor = newColor;
			
			InitializeComponent();

			oldColorLabel.Text = string.Format("Old: {0:X2}", oldColor);
			newColorLabel.Text = string.Format("New: {0:X2}", NewColor);
		}

		private void paletteBox_Paint(object sender, PaintEventArgs e) {
			int x = 0;
			int y = 0;
			for (int i = 0; i < palette.Length; i++) {
				Color c = palette[i];
				e.Graphics.FillRectangle(new SolidBrush(c), x, y, 20, 20);
				if (i == oldColor) {
					e.Graphics.DrawString("O", DefaultFont, new SolidBrush(c.Invert()), x + 5, y + 5);
				} else if (i == NewColor) {
					e.Graphics.DrawString("N", DefaultFont, new SolidBrush(c.Invert()), x + 5, y + 5);
				}

				x += 20;
				if (x >= e.ClipRectangle.Width) {
					y += 20;
					x = 0;
				}
			}
		}

		private void oldColorBox_Paint(object sender, PaintEventArgs e) {
			e.Graphics.FillRectangle(new SolidBrush(palette[oldColor]), e.ClipRectangle);
			e.Graphics.DrawRectangle(Pens.Black, e.ClipRectangle);
		}

		private void newColorBox_Paint(object sender, PaintEventArgs e) {
			e.Graphics.FillRectangle(new SolidBrush(palette[NewColor]), e.ClipRectangle);
			e.Graphics.DrawRectangle(Pens.Black, e.ClipRectangle);
		}

		private void paletteBox_MouseDown(object sender, MouseEventArgs e) {
			SetNewColor((byte)(e.X / 20 + 16 * (e.Y / 20)));
		}

		private void SetNewColor(byte newColor) {
			NewColor = newColor;
			newColorBox.Invalidate();
			paletteBox.Invalidate();
			newColorLabel.Text = string.Format("New: {0:X2}", NewColor);
		}

		private void paletteBox_MouseMove(object sender, MouseEventArgs e) {
			if (e.Button == System.Windows.Forms.MouseButtons.Left) {
				SetNewColor((byte)(e.X / 20 + 16 * (e.Y / 20)));
			}
		}

		private void reset_Click(object sender, EventArgs e) {
			SetNewColor(oldColor);
		}

		private void cancel_Click(object sender, EventArgs e) {
			SetNewColor(originalNewColor);
			Close();
		}

		private void save_Click(object sender, EventArgs e) {
			Close();
		}

		private void paletteBox_DoubleClick(object sender, EventArgs e) {
			Close();
		}
	}
}
