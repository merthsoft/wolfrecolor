﻿namespace WolfReColor {
	partial class NumberRange {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.label1 = new System.Windows.Forms.Label();
			this.fromNumber = new System.Windows.Forms.NumericUpDown();
			this.toNumber = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.okButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.canvas = new System.Windows.Forms.PictureBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.zoomBar = new System.Windows.Forms.TrackBar();
			this.label3 = new System.Windows.Forms.Label();
			this.exportLabel = new System.Windows.Forms.Label();
			this.exportFormatBox = new System.Windows.Forms.ComboBox();
			this.exportImageTypeBox = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.fromNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.toNumber)).BeginInit();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.canvas)).BeginInit();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.zoomBar)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(71, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "From number:";
			// 
			// fromNumber
			// 
			this.fromNumber.Location = new System.Drawing.Point(89, 7);
			this.fromNumber.Name = "fromNumber";
			this.fromNumber.Size = new System.Drawing.Size(120, 20);
			this.fromNumber.TabIndex = 1;
			this.fromNumber.ValueChanged += new System.EventHandler(this.fromNumber_ValueChanged);
			// 
			// toNumber
			// 
			this.toNumber.Location = new System.Drawing.Point(89, 30);
			this.toNumber.Name = "toNumber";
			this.toNumber.Size = new System.Drawing.Size(120, 20);
			this.toNumber.TabIndex = 3;
			this.toNumber.ValueChanged += new System.EventHandler(this.toNumber_ValueChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(22, 32);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(61, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "To number:";
			// 
			// okButton
			// 
			this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.okButton.Location = new System.Drawing.Point(215, 7);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(75, 23);
			this.okButton.TabIndex = 4;
			this.okButton.Text = "OK";
			this.okButton.UseVisualStyleBackColor = true;
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(215, 30);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(75, 23);
			this.cancelButton.TabIndex = 5;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			// 
			// panel1
			// 
			this.panel1.AutoScroll = true;
			this.panel1.Controls.Add(this.canvas);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 110);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(416, 233);
			this.panel1.TabIndex = 6;
			// 
			// canvas
			// 
			this.canvas.Location = new System.Drawing.Point(3, 3);
			this.canvas.Name = "canvas";
			this.canvas.Size = new System.Drawing.Size(64, 64);
			this.canvas.TabIndex = 0;
			this.canvas.TabStop = false;
			this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
			// 
			// panel2
			// 
			this.panel2.AutoScroll = true;
			this.panel2.Controls.Add(this.exportImageTypeBox);
			this.panel2.Controls.Add(this.exportFormatBox);
			this.panel2.Controls.Add(this.cancelButton);
			this.panel2.Controls.Add(this.exportLabel);
			this.panel2.Controls.Add(this.zoomBar);
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.okButton);
			this.panel2.Controls.Add(this.toNumber);
			this.panel2.Controls.Add(this.label2);
			this.panel2.Controls.Add(this.fromNumber);
			this.panel2.Controls.Add(this.label1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(416, 110);
			this.panel2.TabIndex = 7;
			// 
			// zoomBar
			// 
			this.zoomBar.LargeChange = 2;
			this.zoomBar.Location = new System.Drawing.Point(89, 61);
			this.zoomBar.Minimum = 1;
			this.zoomBar.Name = "zoomBar";
			this.zoomBar.Size = new System.Drawing.Size(201, 45);
			this.zoomBar.TabIndex = 7;
			this.zoomBar.Value = 1;
			this.zoomBar.ValueChanged += new System.EventHandler(this.zoomBar_ValueChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(46, 61);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(37, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Zoom:";
			// 
			// exportLabel
			// 
			this.exportLabel.AutoSize = true;
			this.exportLabel.Location = new System.Drawing.Point(296, 7);
			this.exportLabel.Name = "exportLabel";
			this.exportLabel.Size = new System.Drawing.Size(77, 13);
			this.exportLabel.TabIndex = 9;
			this.exportLabel.Text = "Export options:";
			// 
			// exportFormatBox
			// 
			this.exportFormatBox.FormattingEnabled = true;
			this.exportFormatBox.Items.AddRange(new object[] {
            "Bitmap",
            "Png"});
			this.exportFormatBox.Location = new System.Drawing.Point(299, 50);
			this.exportFormatBox.Name = "exportFormatBox";
			this.exportFormatBox.Size = new System.Drawing.Size(105, 21);
			this.exportFormatBox.TabIndex = 10;
			// 
			// exportImageTypeBox
			// 
			this.exportImageTypeBox.FormattingEnabled = true;
			this.exportImageTypeBox.Items.AddRange(new object[] {
            "Multiple images",
            "Single image (long)",
            "Single image (tall)",
            "Single image (square)"});
			this.exportImageTypeBox.Location = new System.Drawing.Point(299, 23);
			this.exportImageTypeBox.Name = "exportImageTypeBox";
			this.exportImageTypeBox.Size = new System.Drawing.Size(105, 21);
			this.exportImageTypeBox.TabIndex = 11;
			this.exportImageTypeBox.SelectedIndexChanged += new System.EventHandler(this.exportImageTypeBox_SelectedIndexChanged);
			// 
			// NumberRange
			// 
			this.AcceptButton = this.okButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(416, 343);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel2);
			this.MinimizeBox = false;
			this.Name = "NumberRange";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Select Range";
			this.Load += new System.EventHandler(this.NumberRange_Load);
			this.Resize += new System.EventHandler(this.NumberRange_Resize);
			((System.ComponentModel.ISupportInitialize)(this.fromNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.toNumber)).EndInit();
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.canvas)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.zoomBar)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown fromNumber;
		private System.Windows.Forms.NumericUpDown toNumber;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.PictureBox canvas;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TrackBar zoomBar;
		private System.Windows.Forms.Label exportLabel;
		private System.Windows.Forms.ComboBox exportImageTypeBox;
		private System.Windows.Forms.ComboBox exportFormatBox;
	}
}