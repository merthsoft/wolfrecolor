﻿using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;

namespace WolfReColor {
	public class PixelMapListBox : ListBox {
		public Color[] Palette { get; set; }

		public PixelMapListBox() {
		}

		protected override void OnDrawItem(DrawItemEventArgs e) {
			// Make sure we're not trying to draw something that isn't there.
			if (e.Index >= this.Items.Count || e.Index <= -1)
				return;

			PixelMap item = (PixelMap)Items[e.Index];
			if (item == null)
				return;

			// Clear out what's there
			e.Graphics.DrawRectangle(Pens.White, e.Bounds);
			e.Graphics.FillRectangle(Brushes.White, e.Bounds);

			// Draw the pixel swatches
			e.Graphics.FillRectangle(new SolidBrush(Palette[item.OldColor]), e.Bounds.X+2, e.Bounds.Y+3, 20, 20);
			e.Graphics.DrawRectangle(Pens.Black, e.Bounds.X + 2, e.Bounds.Y + 3, 20, 20);
			e.Graphics.FillRectangle(new SolidBrush(Palette[item.NewColor]), e.Bounds.X+22, e.Bounds.Y+3, 20, 20);
			e.Graphics.DrawRectangle(Pens.Black, e.Bounds.X+22, e.Bounds.Y+3, 20, 20);

			// Draw the text
			string text = item.ToString();
			SizeF stringSize = e.Graphics.MeasureString(text, this.Font);
			e.Graphics.DrawString(text, this.Font, new SolidBrush(Color.Black),
				new PointF(47, e.Bounds.Y + (e.Bounds.Height - stringSize.Height) / 2));

			if ((e.State & DrawItemState.Selected) == DrawItemState.Selected) {
				// The item is selected.
				// We want a blue background color.
				e.Graphics.DrawRectangle(Pens.Black, e.Bounds);
			}
		}
	}
}
