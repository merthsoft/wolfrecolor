﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WolfReColor {
	public partial class ViewCopiedMappings : Form {
		Dictionary<byte, PixelMap> copiedMaps;
		/// <summary>
		/// Sets the list of copied maps.
		/// </summary>
		public Dictionary<byte, PixelMap> CopiedMaps {
			set {
				if (value == null) { return; }
				copiedMaps = value;
				foreach (PixelMap pm in copiedMaps.Values) {
					pixelMapListBox.Items.Add(pm);
				}
			}
		}

		/// <summary>
		/// Sets the palette.
		/// </summary>
		public Color[] Palette {
			set {
				pixelMapListBox.Palette = value;
			}
		}

		public ViewCopiedMappings() {
			InitializeComponent();
		}
	}
}
