﻿namespace WolfReColor {
	partial class PaletteSwitch {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.listStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.cancelButton = new System.Windows.Forms.Button();
			this.okButton = new System.Windows.Forms.Button();
			this.loadButton = new System.Windows.Forms.Button();
			this.paletteBox = new System.Windows.Forms.PictureBox();
			this.saveListButton = new System.Windows.Forms.Button();
			this.listStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.paletteBox)).BeginInit();
			this.SuspendLayout();
			// 
			// listBox1
			// 
			this.listBox1.ContextMenuStrip = this.listStrip;
			this.listBox1.FormattingEnabled = true;
			this.listBox1.Items.AddRange(new object[] {
            "Wolfenstein 3D",
            "Spear (1)",
            "Spear (2)",
            "Spear (3)",
            "Spear (4)",
            "Spear (5)",
            "Spear (6)",
            "Spear (7)",
            "Spear (8)",
            "Spear (9)"});
			this.listBox1.Location = new System.Drawing.Point(12, 12);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(128, 316);
			this.listBox1.TabIndex = 2;
			this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
			this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
			this.listBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseDown);
			// 
			// listStrip
			// 
			this.listStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
			this.listStrip.Name = "listStrip";
			this.listStrip.Size = new System.Drawing.Size(108, 26);
			this.listStrip.Opening += new System.ComponentModel.CancelEventHandler(this.listStrip_Opening);
			// 
			// deleteToolStripMenuItem
			// 
			this.deleteToolStripMenuItem.Enabled = false;
			this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
			this.deleteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
			this.deleteToolStripMenuItem.Text = "Delete";
			this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(391, 338);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(75, 23);
			this.cancelButton.TabIndex = 3;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			// 
			// okButton
			// 
			this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.okButton.Location = new System.Drawing.Point(310, 338);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(75, 23);
			this.okButton.TabIndex = 4;
			this.okButton.Text = "OK";
			this.okButton.UseVisualStyleBackColor = true;
			// 
			// loadButton
			// 
			this.loadButton.Location = new System.Drawing.Point(12, 338);
			this.loadButton.Name = "loadButton";
			this.loadButton.Size = new System.Drawing.Size(75, 23);
			this.loadButton.TabIndex = 5;
			this.loadButton.Text = "Load PAL";
			this.loadButton.UseVisualStyleBackColor = true;
			this.loadButton.Click += new System.EventHandler(this.loadButton_Click);
			// 
			// paletteBox
			// 
			this.paletteBox.Location = new System.Drawing.Point(146, 12);
			this.paletteBox.Name = "paletteBox";
			this.paletteBox.Size = new System.Drawing.Size(320, 320);
			this.paletteBox.TabIndex = 1;
			this.paletteBox.TabStop = false;
			this.paletteBox.Paint += new System.Windows.Forms.PaintEventHandler(this.paletteBox_Paint);
			// 
			// saveListButton
			// 
			this.saveListButton.Location = new System.Drawing.Point(93, 338);
			this.saveListButton.Name = "saveListButton";
			this.saveListButton.Size = new System.Drawing.Size(75, 23);
			this.saveListButton.TabIndex = 6;
			this.saveListButton.Text = "Save List";
			this.saveListButton.UseVisualStyleBackColor = true;
			this.saveListButton.Click += new System.EventHandler(this.saveListButton_Click);
			// 
			// PaletteSwitch
			// 
			this.AcceptButton = this.okButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(478, 367);
			this.Controls.Add(this.saveListButton);
			this.Controls.Add(this.loadButton);
			this.Controls.Add(this.okButton);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.paletteBox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "PaletteSwitch";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "Switch to What Palette";
			this.listStrip.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.paletteBox)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.PictureBox paletteBox;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Button loadButton;
		private System.Windows.Forms.Button saveListButton;
		private System.Windows.Forms.ContextMenuStrip listStrip;
		private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
	}
}