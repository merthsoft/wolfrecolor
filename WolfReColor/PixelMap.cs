﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace WolfReColor {
	public class PixelMap : IComparable<PixelMap> {
		public byte OldColor { get; set; }
		public byte NewColor { get; set; }
		public List<MapPoint> Points { get; set; }

		private PixelMap() {
			Points = new List<MapPoint>();
		}

		public PixelMap(byte oldColor) : this() {
			OldColor = oldColor;
			NewColor = oldColor;
		}

		public PixelMap(byte oldColor, byte newColor) : this() {
			OldColor = oldColor;
			NewColor = newColor;
		}

		public int CompareTo(PixelMap other) {
			return OldColor.CompareTo(other.OldColor);
		}

		public override string ToString() {
			StringBuilder sb = new StringBuilder(string.Format("{0:X2} => {1:X2}", OldColor, NewColor));
			if (Points.Count > 0) {
				sb.AppendFormat("  ({0})", Points.Count);
			}
			return sb.ToString();
		}
	}
}
