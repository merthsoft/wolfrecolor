﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WolfReColor {
	/// <summary>
	/// A sound in the vswap.
	/// </summary>
	class SoundPage {
		/// <summary>
		/// The compressed data.
		/// </summary>
		byte[] compressedData;

		/// <summary>
		/// Gets the sound data.
		/// </summary>
		public byte[] CompressedData {
			get { return compressedData; }
		}

		/// <summary>
		/// Creates a new SoundPage from the given BinaryReader.
		/// </summary>
		/// <param name="br">The BinaryReader to read from</param>
		/// <param name="size">The size of the sound.</param>
		public SoundPage(BinaryReader br, int size) {
			compressedData = br.ReadArray<byte>(size);
		}
	}
}
