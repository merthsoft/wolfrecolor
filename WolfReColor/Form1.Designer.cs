﻿namespace WolfReColor {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.switchPaletteToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.overwriteClipboardMenutoolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.pasteMappingAcrossToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.clearClipbaordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.resetMappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.commitMappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.gradietMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.viewMappingtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.aboutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.spriteBar = new System.Windows.Forms.HScrollBar();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.wallsTabPage = new System.Windows.Forms.TabPage();
			this.panel3 = new System.Windows.Forms.Panel();
			this.wallCanvas = new System.Windows.Forms.PictureBox();
			this.wallBar = new System.Windows.Forms.HScrollBar();
			this.wallNumberLabel = new System.Windows.Forms.Label();
			this.spritesTabPage = new System.Windows.Forms.TabPage();
			this.panel2 = new System.Windows.Forms.Panel();
			this.spriteCanvas = new System.Windows.Forms.PictureBox();
			this.spriteNumberLabel = new System.Windows.Forms.Label();
			this.pixelMapListBox = new WolfReColor.PixelMapListBox();
			this.pixelMapContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.highlightPixelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolsPanel = new System.Windows.Forms.Panel();
			this.excludedPixelsCheckbox = new System.Windows.Forms.CheckBox();
			this.zoomBar = new System.Windows.Forms.TrackBar();
			this.pasteMappingButton = new System.Windows.Forms.Button();
			this.copyMappingButton = new System.Windows.Forms.Button();
			this.updateCheckBox = new System.Windows.Forms.CheckBox();
			this.saveMappingButton = new System.Windows.Forms.Button();
			this.resetMappingButton = new System.Windows.Forms.Button();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.wallsTabPage.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.wallCanvas)).BeginInit();
			this.spritesTabPage.SuspendLayout();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.spriteCanvas)).BeginInit();
			this.pixelMapContextMenu.SuspendLayout();
			this.toolsPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.zoomBar)).BeginInit();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem1});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(677, 24);
			this.menuStrip1.TabIndex = 0;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.switchPaletteToolStripMenuItem2,
            this.toolStripSeparator,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator2,
            this.exportToolStripMenuItem,
            this.toolStripSeparator3,
            this.exitToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
			this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
			this.openToolStripMenuItem.Text = "&Open";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.openVSAWPToolStripMenuItem_Click);
			// 
			// switchPaletteToolStripMenuItem2
			// 
			this.switchPaletteToolStripMenuItem2.Name = "switchPaletteToolStripMenuItem2";
			this.switchPaletteToolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
			this.switchPaletteToolStripMenuItem2.Size = new System.Drawing.Size(193, 22);
			this.switchPaletteToolStripMenuItem2.Text = "S&witch Palette";
			this.switchPaletteToolStripMenuItem2.Click += new System.EventHandler(this.loadPaletteToolStripMenuItem_Click);
			// 
			// toolStripSeparator
			// 
			this.toolStripSeparator.Name = "toolStripSeparator";
			this.toolStripSeparator.Size = new System.Drawing.Size(190, 6);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Enabled = false;
			this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
			this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
			this.saveToolStripMenuItem.Text = "&Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveVSWAPToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.Enabled = false;
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.S)));
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
			this.saveAsToolStripMenuItem.Text = "Save &As";
			this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveVswapAsToolStripMenuItem_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(190, 6);
			// 
			// exportToolStripMenuItem
			// 
			this.exportToolStripMenuItem.Enabled = false;
			this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
			this.exportToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
			this.exportToolStripMenuItem.Text = "&Export";
			this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(190, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
			this.exitToolStripMenuItem.Text = "E&xit";
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem,
            this.overwriteClipboardMenutoolStripMenuItem2,
            this.pasteToolStripMenuItem,
            this.pasteMappingAcrossToolStripMenuItem1,
            this.toolStripSeparator1,
            this.clearClipbaordToolStripMenuItem,
            this.toolStripSeparator5,
            this.resetMappingToolStripMenuItem,
            this.commitMappingToolStripMenuItem});
			this.editToolStripMenuItem.Enabled = false;
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "&Edit";
			// 
			// copyToolStripMenuItem
			// 
			this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
			this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
			this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
			this.copyToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
			this.copyToolStripMenuItem.Text = "&Copy Mapping";
			this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyMappingButton_Click);
			// 
			// overwriteClipboardMenutoolStripMenuItem2
			// 
			this.overwriteClipboardMenutoolStripMenuItem2.Name = "overwriteClipboardMenutoolStripMenuItem2";
			this.overwriteClipboardMenutoolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
			this.overwriteClipboardMenutoolStripMenuItem2.Size = new System.Drawing.Size(248, 22);
			this.overwriteClipboardMenutoolStripMenuItem2.Text = "&Overwrite Clipboard";
			this.overwriteClipboardMenutoolStripMenuItem2.Click += new System.EventHandler(this.overwriteClipboardToolStripMenuItem_Click);
			// 
			// pasteToolStripMenuItem
			// 
			this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
			this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
			this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
			this.pasteToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
			this.pasteToolStripMenuItem.Text = "&Paste Mapping";
			this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteMappingButton_Click);
			// 
			// pasteMappingAcrossToolStripMenuItem1
			// 
			this.pasteMappingAcrossToolStripMenuItem1.Name = "pasteMappingAcrossToolStripMenuItem1";
			this.pasteMappingAcrossToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
			this.pasteMappingAcrossToolStripMenuItem1.Size = new System.Drawing.Size(248, 22);
			this.pasteMappingAcrossToolStripMenuItem1.Text = "Paste Mapping &Across";
			this.pasteMappingAcrossToolStripMenuItem1.Click += new System.EventHandler(this.pasteMappingAcrossToolStripMenuItem1_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(245, 6);
			// 
			// clearClipbaordToolStripMenuItem
			// 
			this.clearClipbaordToolStripMenuItem.Name = "clearClipbaordToolStripMenuItem";
			this.clearClipbaordToolStripMenuItem.ShortcutKeyDisplayString = "Ctrl+X";
			this.clearClipbaordToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
			this.clearClipbaordToolStripMenuItem.Text = "C&lear Mapping Clipboard";
			this.clearClipbaordToolStripMenuItem.Click += new System.EventHandler(this.clearClipbaordToolStripMenuItem_Click);
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(245, 6);
			// 
			// resetMappingToolStripMenuItem
			// 
			this.resetMappingToolStripMenuItem.Name = "resetMappingToolStripMenuItem";
			this.resetMappingToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
			this.resetMappingToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
			this.resetMappingToolStripMenuItem.Text = "&Reset Mapping";
			this.resetMappingToolStripMenuItem.Click += new System.EventHandler(this.resetMappingButton_Click);
			// 
			// commitMappingToolStripMenuItem
			// 
			this.commitMappingToolStripMenuItem.Name = "commitMappingToolStripMenuItem";
			this.commitMappingToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
			this.commitMappingToolStripMenuItem.Size = new System.Drawing.Size(248, 22);
			this.commitMappingToolStripMenuItem.Text = "Commi&t Mapping";
			this.commitMappingToolStripMenuItem.Click += new System.EventHandler(this.saveMappingButton_Click);
			// 
			// toolsToolStripMenuItem
			// 
			this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gradietMapToolStripMenuItem,
            this.viewMappingtoolStripMenuItem});
			this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
			this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
			this.toolsToolStripMenuItem.Text = "&Tools";
			// 
			// gradietMapToolStripMenuItem
			// 
			this.gradietMapToolStripMenuItem.Name = "gradietMapToolStripMenuItem";
			this.gradietMapToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
			this.gradietMapToolStripMenuItem.Size = new System.Drawing.Size(246, 22);
			this.gradietMapToolStripMenuItem.Text = "&Gradiet Map";
			this.gradietMapToolStripMenuItem.Click += new System.EventHandler(this.gradietMapToolStripMenuItem_Click);
			// 
			// viewMappingtoolStripMenuItem
			// 
			this.viewMappingtoolStripMenuItem.Name = "viewMappingtoolStripMenuItem";
			this.viewMappingtoolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
			this.viewMappingtoolStripMenuItem.Size = new System.Drawing.Size(246, 22);
			this.viewMappingtoolStripMenuItem.Text = "&View Mapping Clipboard";
			this.viewMappingtoolStripMenuItem.Click += new System.EventHandler(this.viewMappingtoolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem1
			// 
			this.helpToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem1});
			this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
			this.helpToolStripMenuItem1.Size = new System.Drawing.Size(44, 20);
			this.helpToolStripMenuItem1.Text = "&Help";
			// 
			// aboutToolStripMenuItem1
			// 
			this.aboutToolStripMenuItem1.Name = "aboutToolStripMenuItem1";
			this.aboutToolStripMenuItem1.Size = new System.Drawing.Size(116, 22);
			this.aboutToolStripMenuItem1.Text = "&About...";
			this.aboutToolStripMenuItem1.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
			// 
			// spriteBar
			// 
			this.spriteBar.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.spriteBar.LargeChange = 1;
			this.spriteBar.Location = new System.Drawing.Point(3, 486);
			this.spriteBar.Maximum = 0;
			this.spriteBar.Name = "spriteBar";
			this.spriteBar.Size = new System.Drawing.Size(484, 17);
			this.spriteBar.TabIndex = 2;
			this.spriteBar.TabStop = true;
			this.spriteBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.spriteBar_Scroll);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 24);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.pixelMapListBox);
			this.splitContainer1.Panel2.Controls.Add(this.toolsPanel);
			this.splitContainer1.Size = new System.Drawing.Size(677, 532);
			this.splitContainer1.SplitterDistance = 498;
			this.splitContainer1.TabIndex = 3;
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.wallsTabPage);
			this.tabControl1.Controls.Add(this.spritesTabPage);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Enabled = false;
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(498, 532);
			this.tabControl1.TabIndex = 2;
			this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
			// 
			// wallsTabPage
			// 
			this.wallsTabPage.Controls.Add(this.panel3);
			this.wallsTabPage.Controls.Add(this.wallBar);
			this.wallsTabPage.Controls.Add(this.wallNumberLabel);
			this.wallsTabPage.Location = new System.Drawing.Point(4, 22);
			this.wallsTabPage.Name = "wallsTabPage";
			this.wallsTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.wallsTabPage.Size = new System.Drawing.Size(490, 506);
			this.wallsTabPage.TabIndex = 0;
			this.wallsTabPage.Text = "Walls";
			this.wallsTabPage.UseVisualStyleBackColor = true;
			// 
			// panel3
			// 
			this.panel3.AutoScroll = true;
			this.panel3.BackColor = System.Drawing.Color.Transparent;
			this.panel3.Controls.Add(this.wallCanvas);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(3, 16);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(484, 470);
			this.panel3.TabIndex = 6;
			// 
			// wallCanvas
			// 
			this.wallCanvas.Cursor = System.Windows.Forms.Cursors.Arrow;
			this.wallCanvas.Location = new System.Drawing.Point(0, 0);
			this.wallCanvas.Name = "wallCanvas";
			this.wallCanvas.Size = new System.Drawing.Size(219, 177);
			this.wallCanvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.wallCanvas.TabIndex = 4;
			this.wallCanvas.TabStop = false;
			this.wallCanvas.Paint += new System.Windows.Forms.PaintEventHandler(this.wallCanvas_Paint);
			this.wallCanvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseDown);
			this.wallCanvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseMove);
			this.wallCanvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseUp);
			// 
			// wallBar
			// 
			this.wallBar.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.wallBar.LargeChange = 1;
			this.wallBar.Location = new System.Drawing.Point(3, 486);
			this.wallBar.Maximum = 0;
			this.wallBar.Name = "wallBar";
			this.wallBar.Size = new System.Drawing.Size(484, 17);
			this.wallBar.TabIndex = 5;
			this.wallBar.TabStop = true;
			this.wallBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.wallBar_Scroll);
			// 
			// wallNumberLabel
			// 
			this.wallNumberLabel.AutoSize = true;
			this.wallNumberLabel.Dock = System.Windows.Forms.DockStyle.Top;
			this.wallNumberLabel.Location = new System.Drawing.Point(3, 3);
			this.wallNumberLabel.Name = "wallNumberLabel";
			this.wallNumberLabel.Size = new System.Drawing.Size(61, 13);
			this.wallNumberLabel.TabIndex = 3;
			this.wallNumberLabel.Text = "Wall #: 0/0";
			this.wallNumberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// spritesTabPage
			// 
			this.spritesTabPage.Controls.Add(this.panel2);
			this.spritesTabPage.Controls.Add(this.spriteBar);
			this.spritesTabPage.Controls.Add(this.spriteNumberLabel);
			this.spritesTabPage.Location = new System.Drawing.Point(4, 22);
			this.spritesTabPage.Name = "spritesTabPage";
			this.spritesTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.spritesTabPage.Size = new System.Drawing.Size(490, 506);
			this.spritesTabPage.TabIndex = 1;
			this.spritesTabPage.Text = "Sprites";
			this.spritesTabPage.UseVisualStyleBackColor = true;
			// 
			// panel2
			// 
			this.panel2.AutoScroll = true;
			this.panel2.Controls.Add(this.spriteCanvas);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(3, 16);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(484, 470);
			this.panel2.TabIndex = 3;
			// 
			// spriteCanvas
			// 
			this.spriteCanvas.Cursor = System.Windows.Forms.Cursors.Arrow;
			this.spriteCanvas.Location = new System.Drawing.Point(0, 0);
			this.spriteCanvas.Name = "spriteCanvas";
			this.spriteCanvas.Size = new System.Drawing.Size(219, 177);
			this.spriteCanvas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.spriteCanvas.TabIndex = 1;
			this.spriteCanvas.TabStop = false;
			this.spriteCanvas.Paint += new System.Windows.Forms.PaintEventHandler(this.spriteCanvas_Paint);
			this.spriteCanvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseDown);
			this.spriteCanvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseMove);
			this.spriteCanvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseUp);
			// 
			// spriteNumberLabel
			// 
			this.spriteNumberLabel.AutoSize = true;
			this.spriteNumberLabel.Dock = System.Windows.Forms.DockStyle.Top;
			this.spriteNumberLabel.Location = new System.Drawing.Point(3, 3);
			this.spriteNumberLabel.Name = "spriteNumberLabel";
			this.spriteNumberLabel.Size = new System.Drawing.Size(67, 13);
			this.spriteNumberLabel.TabIndex = 0;
			this.spriteNumberLabel.Text = "Sprite #: 0/0";
			this.spriteNumberLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pixelMapListBox
			// 
			this.pixelMapListBox.ContextMenuStrip = this.pixelMapContextMenu;
			this.pixelMapListBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pixelMapListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
			this.pixelMapListBox.FormattingEnabled = true;
			this.pixelMapListBox.ItemHeight = 26;
			this.pixelMapListBox.Location = new System.Drawing.Point(0, 0);
			this.pixelMapListBox.Name = "pixelMapListBox";
			this.pixelMapListBox.Palette = null;
			this.pixelMapListBox.ScrollAlwaysVisible = true;
			this.pixelMapListBox.Size = new System.Drawing.Size(175, 407);
			this.pixelMapListBox.TabIndex = 1;
			this.pixelMapListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pixelMapListBox_MouseDoubleClick);
			// 
			// pixelMapContextMenu
			// 
			this.pixelMapContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.highlightPixelsToolStripMenuItem});
			this.pixelMapContextMenu.Name = "pixelMapContextMenu";
			this.pixelMapContextMenu.Size = new System.Drawing.Size(157, 26);
			// 
			// highlightPixelsToolStripMenuItem
			// 
			this.highlightPixelsToolStripMenuItem.Name = "highlightPixelsToolStripMenuItem";
			this.highlightPixelsToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
			this.highlightPixelsToolStripMenuItem.Text = "Highlight Pixels";
			this.highlightPixelsToolStripMenuItem.Click += new System.EventHandler(this.highlightPixelsToolStripMenuItem_Click);
			// 
			// toolsPanel
			// 
			this.toolsPanel.AutoScroll = true;
			this.toolsPanel.Controls.Add(this.excludedPixelsCheckbox);
			this.toolsPanel.Controls.Add(this.zoomBar);
			this.toolsPanel.Controls.Add(this.pasteMappingButton);
			this.toolsPanel.Controls.Add(this.copyMappingButton);
			this.toolsPanel.Controls.Add(this.updateCheckBox);
			this.toolsPanel.Controls.Add(this.saveMappingButton);
			this.toolsPanel.Controls.Add(this.resetMappingButton);
			this.toolsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.toolsPanel.Enabled = false;
			this.toolsPanel.Location = new System.Drawing.Point(0, 407);
			this.toolsPanel.Name = "toolsPanel";
			this.toolsPanel.Size = new System.Drawing.Size(175, 125);
			this.toolsPanel.TabIndex = 2;
			// 
			// excludedPixelsCheckbox
			// 
			this.excludedPixelsCheckbox.AutoSize = true;
			this.excludedPixelsCheckbox.Checked = true;
			this.excludedPixelsCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.excludedPixelsCheckbox.Location = new System.Drawing.Point(91, 53);
			this.excludedPixelsCheckbox.Name = "excludedPixelsCheckbox";
			this.excludedPixelsCheckbox.Size = new System.Drawing.Size(78, 17);
			this.excludedPixelsCheckbox.TabIndex = 5;
			this.excludedPixelsCheckbox.Text = "Show excl.";
			this.excludedPixelsCheckbox.UseVisualStyleBackColor = true;
			this.excludedPixelsCheckbox.CheckedChanged += new System.EventHandler(this.excludedPixelsCheckbox_CheckedChanged);
			// 
			// zoomBar
			// 
			this.zoomBar.LargeChange = 2;
			this.zoomBar.Location = new System.Drawing.Point(3, 6);
			this.zoomBar.Minimum = 1;
			this.zoomBar.Name = "zoomBar";
			this.zoomBar.Size = new System.Drawing.Size(170, 45);
			this.zoomBar.TabIndex = 4;
			this.zoomBar.Value = 6;
			this.zoomBar.Scroll += new System.EventHandler(this.zoomBar_Scroll);
			// 
			// pasteMappingButton
			// 
			this.pasteMappingButton.Location = new System.Drawing.Point(3, 99);
			this.pasteMappingButton.Name = "pasteMappingButton";
			this.pasteMappingButton.Size = new System.Drawing.Size(82, 23);
			this.pasteMappingButton.TabIndex = 3;
			this.pasteMappingButton.Text = "Paste";
			this.pasteMappingButton.UseVisualStyleBackColor = true;
			this.pasteMappingButton.Click += new System.EventHandler(this.pasteMappingButton_Click);
			// 
			// copyMappingButton
			// 
			this.copyMappingButton.AutoSize = true;
			this.copyMappingButton.Location = new System.Drawing.Point(91, 99);
			this.copyMappingButton.Name = "copyMappingButton";
			this.copyMappingButton.Size = new System.Drawing.Size(82, 23);
			this.copyMappingButton.TabIndex = 2;
			this.copyMappingButton.Text = "Copy";
			this.copyMappingButton.UseVisualStyleBackColor = true;
			this.copyMappingButton.Click += new System.EventHandler(this.copyMappingButton_Click);
			// 
			// updateCheckBox
			// 
			this.updateCheckBox.AutoSize = true;
			this.updateCheckBox.Checked = true;
			this.updateCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.updateCheckBox.Location = new System.Drawing.Point(3, 53);
			this.updateCheckBox.Name = "updateCheckBox";
			this.updateCheckBox.Size = new System.Drawing.Size(89, 17);
			this.updateCheckBox.TabIndex = 0;
			this.updateCheckBox.Text = "Update sprite";
			this.updateCheckBox.UseVisualStyleBackColor = true;
			this.updateCheckBox.CheckedChanged += new System.EventHandler(this.updateCheckBox_CheckedChanged);
			// 
			// saveMappingButton
			// 
			this.saveMappingButton.Location = new System.Drawing.Point(91, 73);
			this.saveMappingButton.Name = "saveMappingButton";
			this.saveMappingButton.Size = new System.Drawing.Size(82, 23);
			this.saveMappingButton.TabIndex = 2;
			this.saveMappingButton.Text = "Commit";
			this.saveMappingButton.UseVisualStyleBackColor = true;
			this.saveMappingButton.Click += new System.EventHandler(this.saveMappingButton_Click);
			// 
			// resetMappingButton
			// 
			this.resetMappingButton.Location = new System.Drawing.Point(3, 73);
			this.resetMappingButton.Name = "resetMappingButton";
			this.resetMappingButton.Size = new System.Drawing.Size(82, 23);
			this.resetMappingButton.TabIndex = 1;
			this.resetMappingButton.Text = "Reset";
			this.resetMappingButton.UseVisualStyleBackColor = true;
			this.resetMappingButton.Click += new System.EventHandler(this.resetMappingButton_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(677, 556);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.menuStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Form1";
			this.Text = "Wolf3D Sprite Recolor";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.wallsTabPage.ResumeLayout(false);
			this.wallsTabPage.PerformLayout();
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.wallCanvas)).EndInit();
			this.spritesTabPage.ResumeLayout(false);
			this.spritesTabPage.PerformLayout();
			this.panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.spriteCanvas)).EndInit();
			this.pixelMapContextMenu.ResumeLayout(false);
			this.toolsPanel.ResumeLayout(false);
			this.toolsPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.zoomBar)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.PictureBox spriteCanvas;
		private System.Windows.Forms.HScrollBar spriteBar;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private PixelMapListBox pixelMapListBox;
		private System.Windows.Forms.ContextMenuStrip pixelMapContextMenu;
		private System.Windows.Forms.ToolStripMenuItem highlightPixelsToolStripMenuItem;
		private System.Windows.Forms.Panel toolsPanel;
		private System.Windows.Forms.CheckBox updateCheckBox;
		private System.Windows.Forms.Button pasteMappingButton;
		private System.Windows.Forms.Button saveMappingButton;
		private System.Windows.Forms.Button resetMappingButton;
		private System.Windows.Forms.Label spriteNumberLabel;
		private System.Windows.Forms.Button copyMappingButton;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage wallsTabPage;
		private System.Windows.Forms.TabPage spritesTabPage;
		private System.Windows.Forms.Label wallNumberLabel;
		private System.Windows.Forms.PictureBox wallCanvas;
		private System.Windows.Forms.HScrollBar wallBar;
		private System.Windows.Forms.TrackBar zoomBar;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.CheckBox excludedPixelsCheckbox;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem switchPaletteToolStripMenuItem2;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
		private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripMenuItem resetMappingToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem commitMappingToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem pasteMappingAcrossToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem viewMappingtoolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem gradietMapToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem clearClipbaordToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem overwriteClipboardMenutoolStripMenuItem2;
	}
}

