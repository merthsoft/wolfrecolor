﻿using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System;
using System.Windows.Forms;

namespace WolfReColor {
	/// <summary>
	/// A page of data in the vswap. Used for sprites and textures.
	/// </summary>
	public abstract class DataPage {
		/// <summary>
		/// The byte value of each pixel in the image.
		/// </summary>
		public byte[,] ImageData {
			get {
				byte[,] ret = new byte[64, 64];
				for (int i = 0; i < 64; i++) {
					for (int j = 0; j < 64; j++) {
						ret[i, j] = 255;
					}
				}

				foreach (PixelMap pm in PixelMaps.Values) {
					foreach (MapPoint mp in pm.Points) {
						ret[mp.Y, mp.X] = pm.OldColor;
					}
				}

				return ret;
			}
		}

		/// <summary>
		/// The palette data.
		/// </summary>
		public Color[] Palette { get; set; }

		/// <summary>
		/// Maps the pixel data to a list of the pixels that have that data.
		/// </summary>
		public SortedDictionary<byte, PixelMap> PixelMaps { get; private set; }

		/// <summary>
		/// Gets or sets which pixel should be highlighted. If null, nothing is highlighted.
		/// </summary>
		public byte? HighlightedPixel { get; set; }

		/// <summary>
		/// Gets the compressed data as an array of bytes.
		/// </summary>
		public abstract byte[] CompressedData { get; }

		/// <summary>
		/// Creates a new DataPage with the given palette.
		/// </summary>
		/// <param name="palette">The palette to use for this DataPage.</param>
		public DataPage(Color[] palette) {
			this.Palette = palette;
			PixelMaps = new SortedDictionary<byte, PixelMap>();
		}

		/// <summary>
		/// Gets the image as a BitMap.
		/// </summary>
		/// <param name="showMapping">True to show the proposed mapping. False to show the original image.</param>
		/// <param name="showHighlight">True to show the highlighing. False to not.</param>
		/// /// <param name="showExculded">True to highlight the excluded pixels. False to not.</param>
		/// <returns>A BitMap of the image.</returns>
		public Bitmap GetImage(bool showMapping, bool showHighlight, bool showExculded, Dictionary<byte, PixelMap> previewMapping = null) {
			Bitmap b = new Bitmap(64, 64);
			Bitmap excluded = new Bitmap(64, 64);
			using (Graphics g = Graphics.FromImage(b), g2 = Graphics.FromImage(excluded)) {
				g.FillRectangle(new SolidBrush(Palette[255]), 0, 0, 64, 64);
				g2.FillRectangle(new SolidBrush(Color.FromArgb(0, Color.White)), 0, 0, 64, 64);
			}

			try {
				//var dat = b.LockBits(new Rectangle(0, 0, 64, 64), System.Drawing.Imaging.ImageLockMode.ReadWrite, b.PixelFormat);
				foreach (PixelMap pm in PixelMaps.Values) {
					foreach (MapPoint mp in pm.Points) {
						Color pixelColor = Palette[showMapping && !mp.Excluded ? pm.NewColor : pm.OldColor];
						if (previewMapping != null && previewMapping.ContainsKey(pm.OldColor)) {
							pixelColor = Palette[previewMapping[pm.OldColor].NewColor];
						}
						if (showHighlight && pm.OldColor == HighlightedPixel || showExculded && mp.Excluded) {
							pixelColor = pixelColor.Invert();
						}
						//g.FillRectangle(new SolidBrush(pixelColor), mp.X, mp.Y, 1, 1);
						b.SetPixel(mp.X, mp.Y, pixelColor);
						if (showExculded && mp.Excluded) {
							excluded.SetPixel(mp.X, mp.Y, Color.FromArgb(125, Color.White));
						}
					}
				}
				//b.UnlockBits(dat);
			} catch (Exception e) {
				MessageBox.Show(e.ToString());
			}

			//using (Graphics g = Graphics.FromImage(b)) {
			//    g.DrawImage(excluded, 0, 0);
			//}

			return b;
		}

		public void CommitPixelMaps() {
			SortedDictionary<byte, PixelMap> newMapping = new SortedDictionary<byte, PixelMap>();
			foreach (PixelMap pm in PixelMaps.Values) {
				byte oldColor = pm.OldColor;
				byte newColor = pm.NewColor;
				foreach (MapPoint mp in pm.Points) {
					if (mp.Excluded) {
						mp.Excluded = false;
						if (!newMapping.ContainsKey(oldColor)) { newMapping.Add(oldColor, new PixelMap(oldColor)); }
						newMapping[oldColor].Points.Add(mp);
					} else {
						if (!newMapping.ContainsKey(newColor)) { newMapping.Add(newColor, new PixelMap(newColor)); }
						newMapping[newColor].Points.Add(mp);
					}
				}
			}

			PixelMaps = newMapping;
		}
	}
}
