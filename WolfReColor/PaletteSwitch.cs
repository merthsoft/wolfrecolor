﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace WolfReColor {
	public partial class PaletteSwitch : Form {
		private int selectedPalette;
		/// <summary>
		/// Gets or sets the currently selected palette.
		/// </summary>
		public int SelectedPalette {
			get {
				return selectedPalette;
			}
			set {
				selectedPalette = value;
				listBox1.SelectedIndex = selectedPalette;
				byte[] data;
				switch (selectedPalette) {
					case 0:
						data = Properties.Resources.Wolfenstein3D;
						break;
					case 1:
						data = Properties.Resources.Spear_of_Destiny_1_;
						break;
					case 2:
						data = Properties.Resources.Spear_of_Destiny_2_;
						break;
					case 3:
						data = Properties.Resources.Spear_of_Destiny_3_;
						break;
					case 4:
						data = Properties.Resources.Spear_of_Destiny_4_;
						break;
					case 5:
						data = Properties.Resources.Spear_of_Destiny_5_;
						break;
					case 6:
						data = Properties.Resources.Spear_of_Destiny_6_;
						break;
					case 7:
						data = Properties.Resources.Spear_of_Destiny_7_;
						break;
					case 8:
						data = Properties.Resources.Spear_of_Destiny_8_;
						break;
					case 9:
						data = Properties.Resources.Spear_of_Destiny_9_;
						break;
					case 10:
						return;
					default:
						data = loadedPalettes[selectedPalette - 11];
						break;
				}

				loadPalette(data);
			}
		}

		/// <summary>
		/// Gets the selected palette.
		/// </summary>
		public Color[] Palette { get; private set; }

		List<byte[]> loadedPalettes;

		public PaletteSwitch() {
			InitializeComponent();

			loadedPalettes = new List<byte[]>();
			SelectedPalette = 0;
			if (!File.Exists("palettes")) { return; }
			try {
				using (FileStream fs = new FileStream("palettes", FileMode.Open)) {
					byte[] arr = new byte[4];
					int sel;
					fs.Read(arr, 0, 4);
					sel = BitConverter.ToInt32(arr, 0);
					while (fs.Position < fs.Length) {
						StringBuilder name = new StringBuilder();
						arr = new byte[2];
						fs.Read(arr, 0, 2);
						char c = Encoding.Unicode.GetChars(arr)[0];
						while (c != 0) {
							name.Append(c);
							fs.Read(arr, 0, 2);
							c = Encoding.Unicode.GetChars(arr)[0];
						}
						arr = new byte[768];
						fs.Read(arr, 0, 768);
						loadedPalettes.Add((byte[])arr.Clone());
						if (listBox1.Items.Count == 10) {
							listBox1.Items.Add("--Custom palettes:--");
						}
						listBox1.Items.Add(name.ToString());
					}
					SelectedPalette = sel;
				}
			} catch (Exception ex) {
				MessageBox.Show(string.Concat("Failed to load palette file. Please resave the list.", Environment.NewLine, ex.Message), 
					"Error: Could not load file:");
			}
		}

		private void loadPalette(byte[] data) {
			List<Color> p = new List<Color>();
			for (int i = 0; i < data.Length; i += 3) {
				byte red = data[i];
				byte green = data[i + 1];
				byte blue = data[i + 2];
				p.Add(Color.FromArgb(red, green, blue));
			}

			Palette = p.ToArray();

			paletteBox.Invalidate();
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e) {
			if (listBox1.SelectedIndex != -1) {
				SelectedPalette = listBox1.SelectedIndex;
			}
		}

		private void paletteBox_Paint(object sender, PaintEventArgs e) {
			int x = 0;
			int y = 0;
			for (int i = 0; i < Palette.Length; i++) {
				Color c = Palette[i];
				e.Graphics.FillRectangle(new SolidBrush(c), x, y, 20, 20);
				x += 20;
				if (x >= e.ClipRectangle.Width) {
					y += 20;
					x = 0;
				}
			}
		}

		private void loadButton_Click(object sender, EventArgs e) {
			OpenFileDialog ofd = new OpenFileDialog();
			ofd.AddFilter("Palette", "*.PAL", "*.pal");
			if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.Cancel || !File.Exists(ofd.FileName)) {
				return;
			}

			byte[] data = File.ReadAllBytes(ofd.FileName);

			try {
				loadPalette(data);
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Error: Could not load file: " + ofd.FileName);
				return;
			}

			loadedPalettes.Add((byte[])data.Clone());
			if (listBox1.Items.Count == 10) {
				listBox1.Items.Add("--Custom palettes:--");
			}
			listBox1.Items.Add(new FileInfo(ofd.FileName).Name);
			listBox1.SelectedIndex = listBox1.Items.Count - 1;
		}

		private void listStrip_Opening(object sender, CancelEventArgs e) {
			deleteToolStripMenuItem.Enabled = listBox1.SelectedIndex > 10;
		}

		private void listBox1_MouseDown(object sender, MouseEventArgs e) {
			int indexover = listBox1.IndexFromPoint(e.X, e.Y);
			if (indexover >= 0 && indexover < listBox1.Items.Count) {
				listBox1.SelectedIndex = indexover;
			}
			listBox1.Refresh();
		}

		private void deleteToolStripMenuItem_Click(object sender, EventArgs e) {
			int prevPosition = listBox1.SelectedIndex;
			loadedPalettes.RemoveAt(prevPosition - 11);
			listBox1.Items.RemoveAt(prevPosition);
			listBox1.SelectedIndex = prevPosition - 1;
			if (listBox1.Items.Count == 11) {
				listBox1.Items.RemoveAt(10);
				listBox1.SelectedIndex = prevPosition - 2;
			}
		}

		private void saveListButton_Click(object sender, EventArgs e) {
			try {
				using (var stream = new FileStream("palettes", FileMode.Create)) {
					stream.Write(BitConverter.GetBytes(SelectedPalette), 0, 4);
					for (int i = 0; i < loadedPalettes.Count; i++) {
						byte[] fileNameBytes = Encoding.Unicode.GetBytes(listBox1.Items[i + 11].ToString());
						stream.Write(fileNameBytes, 0, fileNameBytes.Length);
						stream.Write(new byte[] {0, 0}, 0, 2);
						stream.Write(loadedPalettes[i], 0, loadedPalettes[i].Length);
					}
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Error: Could not save file.");
				return;
			}
			MessageBox.Show("Palettes saved.", "Success");
		}

		private void listBox1_DoubleClick(object sender, EventArgs e) {
			DialogResult = System.Windows.Forms.DialogResult.OK;
			Close();
		}
	}
}
