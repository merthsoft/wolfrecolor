﻿using System;
using System.Drawing;
using System.IO;
using System.Collections.Generic;

namespace WolfReColor {
	/// <summary>
	/// A wall in the vswap.
	/// </summary>
	class WallPage : DataPage {
		/// <summary>
		/// Creates a new WallPage from the given BinaryReader, using the given palette.
		/// </summary>
		/// <param name="br">The binary reader to read data from.</param>
		/// <param name="size">The size of the entire data.</param>
		/// <param name="palette">The palette.</param>
		public WallPage(BinaryReader br, int size, Color[] palette)
			: base(palette) {
			if (size != 4096) {
				throw new ArgumentException("Size should be 4096.");
			}
			// This shit's a lot easier--no compression for the textures.
			for (int i = 0; i < 64; i++) {
				for (int j = 0; j < 64; j++) {
					byte color = br.ReadByte();
					if (!PixelMaps.ContainsKey(color)) {
						PixelMaps.Add(color, new PixelMap(color));
					}
					MapPoint mp = new MapPoint(i, j);
					PixelMaps[color].Points.Add(mp);
				}
			}
		}

		/// <summary>
		/// Gets the wall data.
		/// </summary>
		public override byte[] CompressedData {
			get {
				List<byte> data = new List<byte>();
				byte[,] imageData = ImageData;
                for (int i = 0; i < 64; i++) {
					for (int j = 0; j < 64; j++) {
						data.Add(imageData[j, i]);
					}
				}

				return data.ToArray();
			}
		}
	}
}
