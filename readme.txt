Wolfenstein 3D Sprite Recolor Tool 
Merthsoft Creations, March 2012 
Shaun M. McFall 
shaunm.mcfall@gmail.com 
 
This is a little tool I made to make recoloring sprites in the game 
Wolfenstein3D easier. It allows you map all pixels of one color to 
another based on the selected palette, so if you wanted, for example, 
to recolor all the greys to green, you could do that very simply. 
 
Hopefully the UI is intuitive, but I'll explain how to do some times 
in case it's not. 
 
Main UI: 
First off, open up a vswap. Once it's open, use the tabs at the top 
(Walls and Sprites) to switch between walls and sprites. Use the bar 
at the bottom to navigate between different walls and sprites. The pane 
at the right is the mapping pane, it shows what colors are mapped where. 
The ticker bar under the mapping pane is the zoom bar, adjust it to 
adjust the zoom of the image. Under that are two check-boxes, Update 
sprite and Show excl. With update sprite checked, it will preview the 
mapping. With show excl. checked it will show the excluded pixels (more 
on that later). Under that are four buttons, Reset, Commit, Paste, 
and Copy. Reset resets the mapping back to how it was. Commit saves the 
mapping internally, so resetting it will reset it back to the new mapping 
(you must commit each mapping on each sprite before saving). Paste and  
Copy are used to copy and paste the mapping across sprites. 
 
Your First Recoloring: 
Select which pixel you want to recolor in the mapping pane, and double 
click on it. The Map Color window will pop up showing you the current  
palette. Left click to select which color it will map to. At the bottom 
you will see the old color next to the new, so you can see how it looks. 
Press save or cancel once you're done to save or cancel the mapping. The 
reset button will reset the "New" color to the color it was when you 
opened the dialog. You can also double click on a color to choose that 
one. Do this for as many colors as you'd like. Once the sprite looks 
how you'd like it to, press the Commit button (or Ctrl+t) to commit 
the changes. You can then edit more or save the vswap (File->Save, or 
Ctrl+s). 
 
Highlighting Pixels: 
If you want to see which pixels in the image have the specific color from 
the mapping pane, right click on the color and select Hightlight Pixels. 
The pixels on the sprite/texture of that color will now invert. Selecting 
it again will unhighlight those pixels (and highlight the selected pixels 
if you have a different color selected). 
 
Copying and Pasting a Mapping: 
Once you've mapped out the new colors for a sprite or texture, you might 
want to copy/paste that mapping to another sprite or texture. You can do 
that with the Copy and Paste buttons, or Ctrl+c and Ctrl+v, respectively. 
Copying a mapping will add to the clipboard, rather than replace it, 
so you can keep copying into it without worrying about losing any colors 
you copied from a previous sprite. The clipboard is shared between all 
sprites and textures. 
In the Edit menu, there are more advanced clipboard options. "Override 
Clipboard" overrides the current clipboard, instead of copying into it 
like copy does. "Paste Mapping Across" lets you paste across multiple 
sprites or textures (more on that later). "Clear Mapping Clipboard" will 
empty the contents of the clipboard. You can also view the current 
clipboard in the Tools menu, under "View Mapping Clipboard". 
 
Pasting Across: 
If you've copied a mapping into the clipboard, you may want to paste it 
across multiple sprites or textures (for example, maybe you make Hans 
Grosse pink, you would want to paste that to all the Hans sprites, this 
makes that quicker). Once copied, go to Edit->Paste Mapping Across 
(Ctrl-F). With the up/down boxes, choose which sprites/textures you want 
to copy across to. Use the zoom bar to adjust the zoom of the preview. 
The preview allows you to see what the sprites/textures will look like  
once the mapping is applied. Press OK to paste, and Cancel to cancel. 
 
Gradient Map: 
Sometimes you know you want to map an entire swatch of the palette to 
another section, you can do this with the Gradient map Tool. Go to 
Tools->Gradient Map (Ctrl+g) to open up the Gradient Map dialog. Left 
click on a color to select where the original gradient will start. Left 
click again to pick the end. You can adjust the start and end wit the 
left click at any time. Right click to pick where the new colors will 
start. Pressing Save will save the mapping, Cancel will cancel. Press 
Reset to where the mapping starts etc. 
 
Switching Palettes: 
You can switch which palette is used with the "Switch Palette" tool. 
Click File->Switch Palette (Ctrl+w) to open the dialog. There are 10 
pre-loaded palettes (Wolf3D and 9 SoD palettes). Click on a palette 
to see the colors on the right. You can load palettes from disk, too, 
with the Load PAL button. Palettes should be in the same format as 
ChaosEdit palettes, so just the RGB values of the colors in bytes. Once 
you've loaded/selected your palette, press Save List to save the list 
of palettes, so you don't have to load them in next time. Click OK 
to select the palette or Cancel to cancel. If you press OK, the program 
will use that selected palette as the default at start up, as well as 
change the current palette. 
 
Excluding Pixels: 
There are times when you want to map only certain pixels of a certain 
color on a sprite/texture (for example, if you want to change the SS 
uniform to red, but don't want to change the eye color). In order to  
control this, you can exclude pixels from the mapping. All you have to 
do is hover your mouse over the sprite/texture, and the cursor should 
change to a pencil. Left click to exclude a pixel, and right click to 
include it once it's been excluded. If a pixel is excluded and you have 
Show excl. checked, it will invert the pixel. Otherwise there will be no 
visible indicator, but it will still be excluded. 
 
That should cover all the things you can do with this tool. If you have 
any questions, feel free to post in the respective topic at either Die 
Hard Wolfers (http://diehardwolfers.areyep.com/) or Cemetech 
(cemtech.net), or email me (shaunm.mcfall@gmail.com). 
 
Thanks: 
Ben Ryves - For his help with both C#, and figuring out the file format 
Adam Biser - For his help with the file format, and being willing to 
answer my questions 
Weregoose - Logo 
ID Software - Obviously, they made the game.